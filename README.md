# Immigration Health Surcharge Forms

## About the Immigration Health Surcharge Forms

This application is for students to check eligibility and apply for immigration health surcharge reimbursement.

## Quick start

### Githooks

Setup githooks - after cloning the repo, add the githooks configuration to your local git config:

`git config --local include.path ../.gitconfig`

There are two githooks, pre-commit and post-commit;



### Prerequisite

1.Check if redis is up and running 

`sudo service redis-server start`


### Building the application

`npm install`

### Running the application

`npm run start:dev`

### Major Revision

`Manually run pipeline, inserting a variable BUMP_LEVEL with the value MAJOR`

### Minor Revision

`Manually run pipeline, inserting a variable BUMP_LEVEL with the value MINOR`

### Deploying To Dev

`For the job Terraform-Plan-Dev, click the cog and insert a variable DEPLOY_VERSION with the value of the new docker image`

## Contributions

We operate a [code of conduct](CODE_OF_CONDUCT.md) for all contributors.

See our [contributing guide](CONTRIBUTING.md) for guidance on how to contribute.

## License

Released under the [Apache 2 license](LICENCE).
