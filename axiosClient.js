const axios = require('axios')
const envBackendUrl = (process.env.BSA_URL)

const BSAGatewayUrl = (envBackendUrl != null) ? envBackendUrl : ''
const axiosClient = axios.create({
  baseURL: BSAGatewayUrl,
  proxy: false,
  timeout: 10000
})

module.exports = axiosClient
