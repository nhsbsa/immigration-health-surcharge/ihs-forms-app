'use strict';
const dotenv = require('dotenv')
const hof = require('hof-govfrontend-v3');
const config = require('./config');
const bodyParser = require('busboy-body-parser');
const { startPageUrl } = require('./configRoutes');
const { sessionTimeOut, sessionTimeOutWarning, finalSessionTimeOutWarning } = require('./apps/common/constants/session-timeout');

dotenv.config();

let appName = 'ihs-forms';


let settings = require('./hof.settings');

settings = Object.assign({}, settings, {
  routes: settings.routes.map(require),
  redis: config.redis,
  path: "/assets",
  ga4TagId: process.env.GA_4_TAG,
  noCache: process.env.NO_CACHE,
  csp: {
    disabled: process.env.DISABLE_CSP === 'true',
    "connect-src": `'self' https://region1.google-analytics.com https://www.google-analytics.com https://directory.spineservices.nhs.uk;`
  }
});

const app = hof(settings);
app.use((req, res, next) => {
  // Set HTML Language
  res.locals.htmlLang = 'en';
  res.locals.appName = appName;
  res.locals.cookieBannerHeaderTxt = 'Apply for your immigration health surcharge reimbursement as a student';
  res.locals.serviceHeaderTitleTxt = 'Apply for your immigration health surcharge reimbursement as a student';

  // session timeout configs
  res.locals.sessionTimeOut = sessionTimeOut;
  res.locals.sessionTimeOutWarning = sessionTimeOutWarning;
  res.locals.finalSessionTimeOutWarning = finalSessionTimeOutWarning;

  // Set feedback and footer links
  res.locals.startPageRedirectUrl = startPageUrl;
  res.locals.helpBaseUrl = '/student-help/';
  res.locals.feedbackUrl = process.env.FEEDBACK_URL;
  res.locals.bodyClasses = 'govuk-frontend-supported';
  res.locals.footerSupportLinks = [
    { path: 'https://www.gov.uk/help?ref=e03bb38e-d034-4188-ab77-d185da316158', property: 'Help', id: 'help-link' },
    { path: '/student-help/cookies', property: 'Cookies', id: 'cookie-link' },
    { path: '/student-help/contact', property: 'Contact', id: 'contact-link' },
    { path: '/student-help/accessibility-statement', property: 'Accessibility statement', id: 'accessibility-link' },
    { path: '/student-help/terms-conditions', property: 'Terms and conditions', id: 'terms-and-conditions-link' },
    { path: '/student-help/privacy-notice', property: 'Privacy', id: 'privacy-link' }
  ];
  next();
});

app.use('/student-claim', (req, res, next) => {
  if (req.session && req.session['hof-wizard-student-claim']
    && req.session['hof-wizard-student-claim'].hasOwnProperty('student-claim-submit-flag')
    && req.url != '/application-complete'
    && req.url != '/clear-session'
    && req.url != '/start?sessionCleared=true') {
    return res.redirect(`/session-timeout`);
  } else {
    next();
  }
});

app.use('/student-eligibility', (req, res, next) => {
  if (req.session && req.session['hof-wizard-student-claim']
    && req.session['hof-wizard-student-claim'].hasOwnProperty('student-claim-submit-flag')
    && req.url != '/start?sessionCleared=true'
    && req.url != '/start'
  ) {
    return res.redirect(`/session-timeout`);
  } else {
    next();
  }
});

app.use(bodyParser({ limit: config.upload.maxFileSize }));

app.use('/session-timeout', (req, res) => {
  res.render('session-timeout', res.locals);
});
app.use('/test-server-error', (req, res) => {
  throw new Error('Test error');
});

module.exports = app;
