'use strict';

process.env.NODE_ENV = 'test';

const chai = require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));
const should = chai.should();
const expect = chai.expect;
const httpMocks = require('node-mocks-http');
const sinon = require('sinon');
const assert = require("assert");

const reqres = require('hof-govfrontend-v3').utils.reqres;

module.exports = {
  chai,
  should,
  expect,
  sinon,
  reqres,
  assert,
  httpMocks
}