const EnterAddressController = require('../../../../apps/student-claim/controllers/enter-address');
const HOFController = require('hof-govfrontend-v3').controller;
const mockData = require('./../../../_mocks_/student-claim/controllers/dependant-details');
const { chai, reqres, sinon } = require('../../../setup');
const AddressModel = require('../../../../apps/student-claim/models/address');
const _ = require('lodash')

describe('EnterAddressController', () => {
  let req;
  let res;
  const next = sinon.stub();
  const callback = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('enterAddressController instance', () => {
    let enterAddressController;

    beforeEach(() => {
      // an instance of the enterAddressController
      enterAddressController = new EnterAddressController({});
    });

    it('extends the hof-form-controller', () => {
      enterAddressController.should.be.instanceof(HOFController);
    });

    it('has locals, process methods', () => {
      enterAddressController.should.have.property('locals').that.is.a('function');
      enterAddressController.should.have.property('process').that.is.a('function');
    });

    describe('locals', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'locals').returns({
          fields: []
        });
        req.sessionModel.get = sinon.stub();
        req.sessionModel.unset = sinon.stub();
      });

      afterEach(() => {
        HOFController.prototype.locals.restore();
      });

      it('calls super.locals', () => {
        enterAddressController.locals(req, res, callback);
        HOFController.prototype.locals.should.have.been.calledOnce
          .and.calledWithExactly(req, res, callback);
      });
    });

    describe('process', () => {
      beforeEach(() => {
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();
        req.sessionModel.unset = sinon.stub();     
      });

      it('calls process', () => {
        enterAddressController.process(req, res,next);      
        next.should.have.been.called;
      });

      it('calls process with dependants in session', () => {
        req.sessionModel.get.withArgs('all-dependants').returns(mockData.allDependants);
        enterAddressController.process(req, res,next);      
        next.should.have.been.called;
      });
    });
  });

});