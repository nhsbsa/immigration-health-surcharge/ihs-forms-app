const DependantDeleteController = require('../../../../apps/student-claim/controllers/delete-dependant');
const HOFController = require('hof-govfrontend-v3').controller;
const { expect, reqres, sinon } = require('../../../setup');
const mockData = require('./../../../_mocks_/student-claim/controllers/dependant-details');
const _ = require('lodash')

describe('DependantDeleteController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('DependantDeleteController instance', () => {
    let dependantDeleteController;
    const dependantId = 'abc123';
    beforeEach(() => {
      // an instance of the DependantDeleteController
      dependantDeleteController = new DependantDeleteController({});
    });

    it('extends the hof-form-controller', () => {
      dependantDeleteController.should.be.instanceof(HOFController);
    });

    it('has get, deleteDependant methods', () => {
      dependantDeleteController.should.have.property('get').that.is.a('function');
      dependantDeleteController.should.have.property('deleteDependant').that.is.a('function');
    });

    describe('get', () => {
      beforeEach(() => {
        dependantDeleteController.deleteDependant = sinon.stub();
      });

      it('calls next() skiping condition if there is no dependantId', () => {
        req.query = {};
        dependantDeleteController.get(req, res, next);
        next.should.have.been.called;
      });

      it('calls deleteDependant when dependantId is available', () => {
        req.query = { dependantId };
        dependantDeleteController.get(req, res, next);
        dependantDeleteController.deleteDependant.should.have.been.calledWithExactly(req, res, dependantId);
      });
    });

    describe('deleteDependant', () => {
      beforeEach(() => {
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();
        req.sessionModel.unset = sinon.stub();
        sessionModel = sinon.stub();
        res.redirect = sinon.stub();
        req.baseUrl = '/student-claim';
        req.session['hof-wizard-health-claim'] = sinon.stub();      
      });    

      it('fileId not found', () => { 
        req.sessionModel.get.returns([{ 'all-dependants': mockData.allDependants },{'dependant-id': dependantId}]);
        const resp = dependantDeleteController.deleteDependant(req, res, dependantId);
        expect(resp).to.be.an.instanceOf(Promise);
       });

      it('Calls redirect to add dependant page, if all dependats are deleted', () => { 
        req.sessionModel.get.returns([{ 'all-dependants': mockData.allDependants }]);
        const resp =dependantDeleteController.deleteDependant(req, res, dependantId);
        res.redirect.should.not.have.been.called;
      });

      it('Calls redirect to check-dependant-answerst page', () => {
        req.sessionModel.get.returns([{ 'all-dependants': mockData.allDependants }]);
        const resp =dependantDeleteController.deleteDependant(req, res, 'xyz');
        expect(resp).to.be.an.instanceOf(Promise);
      });
    });
  });

});