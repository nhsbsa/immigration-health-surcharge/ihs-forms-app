const DependantAddController = require('../../../../apps/student-claim/controllers/add-dependant');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('DependantAddController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('DependantAddController instance', () => {
    let dependantAddController;

    beforeEach(() => {
      // an instance of the DependantAddController
      dependantAddController = new DependantAddController({});
    });

    it('extends the hof-form-controller', () => {
      dependantAddController.should.be.instanceof(HOFController);
    });

    it('has locals, process methods', () => {
      dependantAddController.should.have.property('locals').that.is.a('function');
      dependantAddController.should.have.property('process').that.is.a('function');
    });

    describe('locals', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'locals').returns({
          fields: []
        });
        req.sessionModel.get = sinon.stub();
        req.sessionModel.unset = sinon.stub();
      });
      afterEach(() => {
        HOFController.prototype.locals.restore();
      });

      it('calls super.locals', () => {
        dependantAddController.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });

      it('calls gets deletedDependant from the sessionModel', () => {
        dependantAddController.locals(req, res, next);
        req.sessionModel.get.should.have.been.calledWithExactly('deletedDependant');
      });

      it('calls unsets deletedDependant from the sessionModel', () => {
        dependantAddController.locals(req, res, next);
        req.sessionModel.unset.should.have.been.calledWithExactly('deletedDependant');
      });

      it('sets locals value for deletedDependant to true', () => {
        req.sessionModel.get.returns(true);

        dependantAddController.locals(req, res, next).deletedDependant.should.be.true;
      });
    });

    describe('process', () => {
      let getTimeValue = '7865432645';

      beforeEach(() => {
        sinon.stub(Date.prototype, 'getTime').returns(getTimeValue);
        req.sessionModel.set = sinon.stub();
      });
      afterEach(() => {
        Date.prototype.getTime.restore();
      });

      it('calls set dependant-id with unique value', () => {
        _.set(req, 'form.values', {
          "add-dependants-with-european-health-insurance-card-ehic": 'yes'
        });
        const uniqueId = 'id' + getTimeValue;
        dependantAddController.process(req, res, next);
        req.sessionModel.set.should.have.been.calledOnce
          .and.calledWithExactly('dependant-id', uniqueId);
      });

      it('calls set dependant-id with undefined', () => {
        _.set(req, 'form.values', {
          "add-dependants-with-european-health-insurance-card-ehic": 'no'
        });
        const uniqueId = void 0;
        dependantAddController.process(req, res, next);
        req.sessionModel.set.should.have.been.calledOnce
          .and.calledWithExactly('dependant-id', uniqueId);
      });
    });
  });

});