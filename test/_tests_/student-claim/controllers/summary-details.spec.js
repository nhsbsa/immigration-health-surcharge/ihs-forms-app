const SummaryDetailsController = require('../../../../apps/student-claim/controllers/summary-details');
const mockData = require('./../../../_mocks_/student-claim/controllers/summary-details');
const { moreDependantClearSession } = require('./../../../../apps/student-claim/constants/dependant');
const HOFController = require('hof-govfrontend-v3').controller;
const PersonalDetailsModel = require('./../../../../apps/student-claim/models/personal-details');
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash');

describe('SummaryDetailsController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('SummaryDetailsController instance', () => {
    let summaryDetailsController;
    const dependantId = 'abc123';
    beforeEach(() => {
      // an instance of the SummaryDetailsController
      summaryDetailsController = new SummaryDetailsController({});
    });

    it('extends the hof-form-controller', () => {
      summaryDetailsController.should.be.instanceof(HOFController);
    });

    it('has getValues, locals, saveDependant, genrateDependantDataList, getDisplayValue and process methods', () => {
      summaryDetailsController.should.have.property('getValues').that.is.a('function');
      summaryDetailsController.should.have.property('locals').that.is.a('function');
      summaryDetailsController.should.have.property('savePersonalDetails').that.is.a('function');
      summaryDetailsController.should.have.property('generatePersonalDataList').that.is.a('function');
      summaryDetailsController.should.have.property('genrateDependantDataList').that.is.a('function');
      summaryDetailsController.should.have.property('getDisplayValue').that.is.a('function');
    });

    describe('getValues', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'getValues').returns({
          fields: []
        });
        _.set(summaryDetailsController, 'options.locals.field', {})
        summaryDetailsController.savePersonalDetails = sinon.stub();
        req.sessionModel.toJSON = sinon.stub();
        req.sessionModel.toJSON.returns(mockData.sessionModel);
      });
      afterEach(() => {
        HOFController.prototype.getValues.restore();
      });

      it('calls super.getValues', () => {
        summaryDetailsController.getValues(req, res, next);
        HOFController.prototype.getValues.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });

      it('calls savePersonalDetails', () => {
        summaryDetailsController.getValues(req, res, next);
        summaryDetailsController.savePersonalDetails.should.have.been.calledOnce
          .and.calledWithExactly(req, mockData.sessionModel);
      });
    });

    describe('locals', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'locals').returns({
          fields: []
        });
        req.sessionModel.get = sinon.stub();
        req.sessionModel.get.withArgs('all-dependants').returns(mockData.allDependants);
        req.sessionModel.get.withArgs('personalDetails').returns(mockData.personalDetails);
        summaryDetailsController.genrateDependantDataList = sinon.stub();
        summaryDetailsController.genrateDependantDataList.returns(mockData.displayDependants);
        summaryDetailsController.generatePersonalDataList = sinon.stub();
        summaryDetailsController.generatePersonalDataList.returns(mockData.displayPersonalDetails);
      });
      afterEach(() => {
        HOFController.prototype.locals.restore();
      });

      it('calls super.locals', () => {
        summaryDetailsController.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });

      it('calls genrateDependantDataList with dependants array', () => {
        summaryDetailsController.locals(req, res, next);
        summaryDetailsController.genrateDependantDataList.should.have.been.calledOnce
          .and.calledWithExactly(req, mockData.allDependants);
      });

      it('calls generatePersonalDataList', () => {
        summaryDetailsController.locals(req, res, next);
        summaryDetailsController.generatePersonalDataList.should.have.been.calledOnce
          .and.calledWithExactly(req, mockData.personalDetails);
      });
      
      it('sets personal details to locals', () => {
        summaryDetailsController.locals(req, res, next).personalDetailsDataList.should.deep.equal(mockData.displayPersonalDetails);
      });

      it('sets dependant details to locals', () => {
        summaryDetailsController.locals(req, res, next).dependantsDataList.should.deep.equal(mockData.displayDependants);
      });

      it('sets empty dependant details to locals', () => {
        req.sessionModel.get.withArgs('all-dependants').returns(void 0);
        summaryDetailsController.locals(req, res, next).dependantsDataList.should.deep.equal([])
      });
    });

    describe('savePersonalDetails', () => {
      beforeEach(() => {
        req.sessionModel.set = sinon.stub();
      });

      it('calls set from sessionModel', () => {
        const personalDetails = new PersonalDetailsModel(mockData.sessionModel)
        summaryDetailsController.savePersonalDetails(req, mockData.sessionModel);
        req.sessionModel.set.should.have.been.calledOnce
          .and.calledWithExactly('personalDetails', personalDetails);
      });
    });

    describe('generatePersonalDataList', () => {
      beforeEach(() => {
        req.translate = sinon.stub();
        req.translate.returns('Translated Label');
        summaryDetailsController.getDisplayValue = sinon.stub();
        summaryDetailsController.getDisplayValue.returns('Tranformed Value')
      });

      it('returns correct display values for dependant details', () => {
        summaryDetailsController.generatePersonalDataList(req, mockData.personalDetails).should.deep.equal(mockData.displayPersonalDetails);
      });
    });

    describe('genrateDependantDataList', () => {
      beforeEach(() => {
        req.translate = sinon.stub();
        req.translate.returns('Translated Label');
        summaryDetailsController.getDisplayValue = sinon.stub();
        summaryDetailsController.getDisplayValue.returns('Tranformed Value')
      });

      it('returns correct display details for dependants', () => {
        summaryDetailsController.genrateDependantDataList(req, mockData.allDependants).should.deep.equal(mockData.displayDependants);
      });
    });

    describe('getDisplayValue', () => {
      const translatedValue = 'Translated Value'
      beforeEach(() => {
        req.translate = sinon.stub();
        req.translate.returns(translatedValue);
      });

      it('returns correctly formatted date', () => {
        summaryDetailsController.getDisplayValue(req, 'dependant-date-of-birth', mockData.dateInput).should.equal(mockData.dateOutput);
        summaryDetailsController.getDisplayValue(req, 'date-of-birth', mockData.dateInput).should.equal(mockData.dateOutput);
      });

      it('returns correctly formatted file name', () => {
        summaryDetailsController.getDisplayValue(req, 'upload-dependant-european-health-insurance-card-ehic', mockData.fileName).should.equal(mockData.fileName[0].fileName);
      });

      it('returns correctly formatted file list names', () => {
        summaryDetailsController.getDisplayValue(req, 'applicant-file-upload', mockData.filesList).should.equal(mockData.formattedFileList);
      });

      it('returns correctly translated value', () => {
        summaryDetailsController.getDisplayValue(req, 'applicant-share-code', mockData.dateInput).should.equal(translatedValue);
      });
    });
    
  });
});