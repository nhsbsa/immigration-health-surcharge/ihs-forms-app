const DependantNotEligibleController = require('../../../../apps/student-claim/controllers/dependant-not-eligible');
const { notEligibleClearSession } = require('./../../../../apps/student-claim/constants/dependant');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('DependantNotEligibleController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('DependantNotEligibleController instance', () => {
    let dependantNotEligibleController;

    beforeEach(() => {
      // an instance of the dependantNotEligibleController
      dependantNotEligibleController = new DependantNotEligibleController({});
    });

    it('extends the hof-form-controller', () => {
      dependantNotEligibleController.should.be.instanceof(HOFController);
    });

    it('has a process method', () => {
      dependantNotEligibleController.should.have.property('process').that.is.a('function');
    });

    describe('process', () => {

      beforeEach(() => {
        req.sessionModel.unset = sinon.stub();
      });

      it('calls unset with array of form control names', () => {
        dependantNotEligibleController.process(req, res, next);
        req.sessionModel.unset.should.have.been.calledOnce
          .and.calledWithExactly(notEligibleClearSession);
      });

      it('calls next()', () => {
        dependantNotEligibleController.process(req, res, next);
        next.should.have.been.called;
      });
    });
  });

});