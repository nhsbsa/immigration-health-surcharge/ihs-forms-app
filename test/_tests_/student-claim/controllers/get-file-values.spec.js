const FileDetailsController = require('../../../../apps/student-claim/controllers/get-file-values');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('FileDetailsController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('FileDetailsController instance', () => {
    let fileDetailsController;

    beforeEach(() => {
      // an instance of the FileDetailsController
      fileDetailsController = new FileDetailsController({});
    });

    it('extends the hof-form-controller', () => {
      fileDetailsController.should.be.instanceof(HOFController);
    });

    it('has locals, process methods', () => {
      fileDetailsController.should.have.property('getValues').that.is.a('function');
      fileDetailsController.should.have.property('locals').that.is.a('function');
    });

    describe('getValues', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'getValues').returns({
          fields: []
        });
        _.set(fileDetailsController, 'options.locals.field', {})
      });
      afterEach(() => {
        HOFController.prototype.getValues.restore();
      });

      it('calls super.getValues', () => {
        fileDetailsController.getValues(req, res, next);
        HOFController.prototype.getValues.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });
    });

    describe('locals', () => {
      const fileName1 = 'abc123', filename2 = 'cde456';
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'locals').returns({
          fields: []
        });
        req.sessionModel.get = sinon.stub();
        req.sessionModel.get.withArgs('ehicFile').returns(fileName1);
        req.sessionModel.get.withArgs('studyLetter').returns(filename2);
      });
      afterEach(() => {
        HOFController.prototype.locals.restore();
      });

      it('calls super.locals', () => {
        fileDetailsController.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });

      it('sets locals values with respective file names', () => {
        fileDetailsController.locals(req, res, next).ehicUploadFile.should.equal(fileName1);
        fileDetailsController.locals(req, res, next).studyLetterUploadFile.should.equal(filename2);
      });
    });
  });

});