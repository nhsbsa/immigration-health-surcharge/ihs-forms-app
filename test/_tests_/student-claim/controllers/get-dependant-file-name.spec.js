const DependantFileNameController = require('../../../../apps/student-claim/controllers/get-dependant-file-name');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('DependantFileNameController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('DependantFileNameController instance', () => {
    let dependantFileNameController;
    const dependantId = 'abc123';

    beforeEach(() => {
      // an instance of the DependantFileNameController
      dependantFileNameController = new DependantFileNameController({});
    });

    it('extends the hof-form-controller', () => {
      dependantFileNameController.should.be.instanceof(HOFController);
    });

    it('has locals, process methods', () => {
      dependantFileNameController.should.have.property('getValues').that.is.a('function');
      dependantFileNameController.should.have.property('locals').that.is.a('function');
    });

    describe('getValues', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'getValues').returns({
          fields: []
        });
        _.set(dependantFileNameController, 'options.locals.field', {})
      });
      afterEach(() => {
        HOFController.prototype.getValues.restore();
      });

      it('calls super.getValues', () => {
        dependantFileNameController.getValues(req, res, next);
        HOFController.prototype.getValues.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });
    });

    describe('locals', () => {
      const fileName = 'abc123';
      const allDependants = [];
      const dependantId = '123';
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'locals').returns({
          fields: []
        });
        allDependants.findIndex = sinon.stub();
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();
        req.sessionModel.unset = sinon.stub();
      });
      afterEach(() => {
        HOFController.prototype.locals.restore();
      });
/*
      it('calls super.locals', () => {
        dependantFileNameController.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });
      */
      
      it('it sets dependant-id and calls locals ', () => {
        req.sessionModel.get.returns([{ 'dependant-id': dependantId }]);
        dependantFileNameController.locals(req, res ,next);
      });
      
    });
  });

});