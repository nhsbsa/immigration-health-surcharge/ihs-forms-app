const DependantEditController = require('../../../../apps/student-claim/controllers/edit-dependant');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('DependantEditController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('dependantEditController instance', () => {
    let dependantEditController;
    const dependantId = 'abc123';
    const url = '/dependant-share-code';

    beforeEach(() => {
      // an instance of the dependantEditController
      dependantEditController = new DependantEditController({});
    });

    it('extends the hof-form-controller', () => {
      dependantEditController.should.be.instanceof(HOFController);
    });

    it('has locals, process methods', () => {
      dependantEditController.should.have.property('get').that.is.a('function');
      dependantEditController.should.have.property('editDependant').that.is.a('function');
    });

    describe('get', () => {
      beforeEach(() => {
        dependantEditController.editDependant = sinon.stub();
      });

      it('calls next() skiping condition if there is no dependantId', () => {
        req.query = {};
        dependantEditController.get(req, res, next);
        next.should.have.been.called;
      });

      it('calls get with  values ', () => {
        _.set(req, 'req.query', {     "dependantId": '123' , "pagename":"/dependant-share-code", "checkdep":true });       
        dependantEditController.get(req, res, next);
      });

      it('calls deleteDependant when dependantId is available', () => {
        req.query = { dependantId };
        dependantEditController.get(req, res, next);
      });

    });

    describe('editDependant', () => {
      beforeEach(() => {
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();
        req.sessionModel.unset = sinon.stub();
        res.redirect = sinon.stub();
        req.baseUrl = '/student-claim';
          
      });

      it('skips deleteDependant if dependant with Id not found', () => {
        req.sessionModel.get.returns([]);
        dependantEditController.editDependant(req, res, dependantId,url);
        const nextNav = '/'+url+'?dependantId='+dependantId;        
        res.redirect.should.have.been.calledWithExactly(`${req.baseUrl}${nextNav}`);
      });

      it('Calls redirect to edit dependant page', () => {
        req.sessionModel.get.returns([{ 'dependant-id': dependantId }]);
        dependantEditController.editDependant(req, res, dependantId,url);
        const nextNav = '/'+url+'?dependantId='+dependantId;
        res.redirect.should.have.been.calledWithExactly(`${req.baseUrl}${nextNav}`);
      });

      it('Calls redirect to edit dependant page', () => {
        req.sessionModel.get.returns([{ 'dependant-id': dependantId }, { 'dependant-id': 'cde345' }]);
        dependantEditController.editDependant(req, res, dependantId,url);
        const nextNav = '/'+url+'?dependantId='+dependantId;
        res.redirect.should.have.been.calledWithExactly(`${req.baseUrl}${nextNav}`);
      });
    

      it('calls set checkdep  with dependantId', () => {
        _.set(req, 'query.values', {     "dependantId": '123' , "pagename":"/dependant-share-code", "checkdep":true });       
        dependantEditController.get(req, res, next);
      });

      it('calls set checkdep to false with dependantId and pagename', () => {
        _.set(req, 'query.values', {     "dependantId": '123' , "pagename":"/dependant-share-code", "checkdep":false });       
        dependantEditController.get(req, res, next);
      });

    });
  });

});