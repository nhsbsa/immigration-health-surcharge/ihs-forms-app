const DependantDetailsController = require('../../../../apps/student-claim/controllers/dependant-details');
const mockData = require('./../../../_mocks_/student-claim/controllers/dependant-details');
const { moreDependantClearSession } = require('./../../../../apps/student-claim/constants/dependant');
const HOFController = require('hof-govfrontend-v3').controller;
const DependantModel = require('./../../../../apps/student-claim/models/dependant');
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash');

describe('DependantDetailsController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('DependantDetailsController instance', () => {
    let dependantDetailsController;
    const dependantId = 'abc123';
    beforeEach(() => {
      // an instance of the DependantDetailsController
      dependantDetailsController = new DependantDetailsController({});
    });

    it('extends the hof-form-controller', () => {
      dependantDetailsController.should.be.instanceof(HOFController);
    });

    it('has get and deleteDependant methods', () => {
      dependantDetailsController.should.have.property('getValues').that.is.a('function');
      dependantDetailsController.should.have.property('locals').that.is.a('function');
      dependantDetailsController.should.have.property('genrateDependantDataList').that.is.a('function');
      dependantDetailsController.should.have.property('saveDependant').that.is.a('function');      
      dependantDetailsController.should.have.property('getDisplayValue').that.is.a('function');
      dependantDetailsController.should.have.property('process').that.is.a('function');
    });

    describe('locals', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'locals').returns({
          fields: []
        });
        req.sessionModel.unset = sinon.stub();
        req.sessionModel.get = sinon.stub();
        req.sessionModel.get.withArgs('all-dependants').returns(mockData.allDependants);
        req.sessionModel.get.withArgs('deletedDependant').returns(mockData.deleteDependant);
        dependantDetailsController.genrateDependantDataList = sinon.stub();
        dependantDetailsController.genrateDependantDataList.returns(mockData.displayDependants);
      });
      afterEach(() => {
        HOFController.prototype.locals.restore();
      });

      it('calls super.locals', () => {
        dependantDetailsController.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });

      it('calls genrateDependantDataList', () => {
        dependantDetailsController.locals(req, res, next);
        dependantDetailsController.genrateDependantDataList.should.have.been.calledOnce
          .and.calledWithExactly(req, mockData.allDependants);
      });
      
      it('sets deleteDependant to locals', () => {
        dependantDetailsController.locals(req, res, next).deletedDependant.should.equal(mockData.deleteDependant)
      });
    });

    describe('saveDependant', () => {
      beforeEach(() => {
        req.sessionModel.set = sinon.stub();
      });

      it('calls saveDependant without arguments', () => {
        const sessionModel = {}
        dependantDetailsController.saveDependant(req, sessionModel);
        req.sessionModel.set.should.not.have.been.called;
      });

      it('calls saveDependant with arguments', () => {
        const allDependants = [new DependantModel(mockData.sessionModel)]
        dependantDetailsController.saveDependant(req, mockData.sessionModel);
        req.sessionModel.set.should.have.been.calledOnce.and.calledWithExactly('all-dependants', allDependants);
      });
    });

    describe('genrateDependantDataList', () => {
      beforeEach(() => {
        req.sessionModel.set = sinon.stub();
      });

      it('calls genrateDependantDataList ', () => {        
        dependantDetailsController.genrateDependantDataList(req, mockData.allDependants);
        req.sessionModel.set.should.not.have.been.called;
      });
    });

    describe('process', () => {
      beforeEach(() => {
        req.sessionModel.unset = sinon.stub();
        res.redirect = sinon.stub();
        req.sessionModel.get = sinon.stub();
        req.sessionModel.get.returns = sinon.stub();       
        req.sessionModel.set = sinon.stub();
      });
      
      it('calls next()', () => {
        dependantDetailsController.process(req, res, next);
        next.should.have.been.called;
      });

      it('calls redirect   ', () => {
        req.sessionModel.get.returns([{ 'all-dependants': mockData.allDependants }]);
        req.form.values['add-more-dependants'] = 'yes';
        dependantDetailsController.process(req, res, next);
        res.redirect.should.have.been.called;
      });
    });
  });
});