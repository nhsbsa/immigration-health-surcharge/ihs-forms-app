const SubmitClaimModel = require('../../../../apps/student-claim/models/submit-claim');
const ClaimUtility = require('../../../../apps/student-claim/utility/claim-utility');
const SubmitClaim = require('../../../../apps/student-claim/controllers/submit-claim');

const mockData = require('./../../../_mocks_/student-claim/controllers/submit-claim');

const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash');


describe('Submit Claim Controller', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('Submit Claim Controller instance', () => {
    let submitClaim;
    beforeEach(() => {
      submitClaim = new SubmitClaim({});
    });

    it('extends the hof-form-controller', () => {
      submitClaim.should.be.instanceof(HOFController);
    });
 

    describe('process', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'process').returns({
          fields: []
        });
        req.sessionModel.get = sinon.stub();
        req.sessionModel.set = sinon.stub();    
        req.sessionModel.toJSON = sinon.stub();
        req.sessionModel.toJSON.returns(mockData.sessionModel); 
        req.sessionModel.get = sinon.stub();
        
      });
      afterEach(() => {
        HOFController.prototype.process.restore();
      });

      it('calls process with arguments', () => {
        submitClaim.process(req, res, next);
        const claim = ClaimUtility.updateClaimFromSession(mockData.sessionModel);
        const submitClaimModel = new SubmitClaimModel();
        const response = submitClaimModel.submitClaim(claim,next);
      });
    });
  });
});