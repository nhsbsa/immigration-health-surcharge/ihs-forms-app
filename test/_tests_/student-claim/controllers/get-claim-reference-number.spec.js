const ClaimReferenceNumberController = require('../../../../apps/student-claim/controllers/get-claim-reference-number');
const HOFController = require('hof-govfrontend-v3').controller;
const { chai, reqres, sinon } = require('../../../setup');
const _ = require('lodash')

describe('ClaimReferenceNumberController', () => {
  let req;
  let res;
  const next = sinon.stub();

  beforeEach(() => {
    req = reqres.req();
    res = reqres.res();
  });

  describe('ClaimReferenceNumberController instance', () => {
    let claimReferenceNumberController;

    beforeEach(() => {
      // an instance of the ClaimReferenceNumberController
      claimReferenceNumberController = new ClaimReferenceNumberController({});
    });

    it('extends the hof-form-controller', () => {
      claimReferenceNumberController.should.be.instanceof(HOFController);
    });

    it('has locals, process methods', () => {
      claimReferenceNumberController.should.have.property('getValues').that.is.a('function');
      claimReferenceNumberController.should.have.property('locals').that.is.a('function');
    });

    describe('getValues', () => {
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'getValues').returns({
          fields: []
        });
        _.set(claimReferenceNumberController, 'options.locals.field', {})
      });
      afterEach(() => {
        HOFController.prototype.getValues.restore();
      });

      it('calls super.getValues', () => {
        claimReferenceNumberController.getValues(req, res, next);
        HOFController.prototype.getValues.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });
    });

    describe('locals', () => {
      const referenceNumber = 'abc123';
      beforeEach(() => {
        sinon.stub(HOFController.prototype, 'locals').returns({
          fields: []
        });
        req.sessionModel.get = sinon.stub();
      });
      afterEach(() => {
        HOFController.prototype.locals.restore();
      });

      it('calls super.locals', () => {
        claimReferenceNumberController.locals(req, res, next);
        HOFController.prototype.locals.should.have.been.calledOnce
          .and.calledWithExactly(req, res, next);
      });

      it('sets locals value for claimReferenceNumber to abc123', () => {
        req.sessionModel.get.returns(referenceNumber);
        claimReferenceNumberController.locals(req, res, next).claimReferenceNumber.should.equal(referenceNumber);
      });
    });
  });

});