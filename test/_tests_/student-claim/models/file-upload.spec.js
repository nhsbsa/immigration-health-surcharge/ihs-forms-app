'use strict';
const Model = require('hof-govfrontend-v3').model;
const axios = require('axios').default;
const FileUploadModel = require('../../../../apps/student-claim/models/file-upload');

const { chai, reqres, sinon, expect, assert } = require('../../../setup');

const fileUploadMock = {
  fileuploadAPI() {
    return {
      "status": "200",
      "message": "Record created/updated successfully",
      "success": true
    };
  },
};

describe('File Upload Model', () => {
  let sandbox;
  let req;
  const next = sinon.stub();

  beforeEach(function () {
    req = reqres.req();
    req.sessionModel.get = sinon.stub();    
    sandbox = sinon.createSandbox();
  });

  afterEach(() => sandbox.restore());

  it('returns a promise', () => {
    const claim = sinon.stub();
    const file = sinon.stub();
    const fileUploadModel = new FileUploadModel();
    const response = fileUploadModel.fileuploadAPI(claim,file,next);
    expect(response).to.be.an.instanceOf(Promise);
  });

  it('should throw an error if can\'t upload claim Evidence', () => {
    const fileUploadModel = new FileUploadModel();
    const stub = sinon.stub(fileUploadModel, 'fileuploadAPI').rejects();
    return fileUploadModel.fileuploadAPI(req, next).should.be.rejectedWith('')
      .then(() => {
        stub.restore();
      });
  });

   describe('fileuploadAPI', () => {
    it('returns a promise', () => {
      const fileUploadModel = new FileUploadModel();
      const response = fileUploadModel.fileuploadAPI();
      expect(response).to.be.an.instanceOf(Promise);
    });

    it('throw error if file upload fails', function () {
      const fileUploadModel = new FileUploadModel();
      const error = new Error("some fake error");
      const call = sandbox.stub(fileUploadModel, "fileuploadAPI").throws(error);
      expect(call).to.throw(Error);
    });

    it("makes a call to file upload api", function () {
      const fileUploadModel = new FileUploadModel();
      sinon.stub(fileUploadModel, "fileuploadAPI").callsFake(function fakeFn() {
        return {
          "status": "200",
          "message": "Record created/updated successfully",
          "success": true
        };
      });
      assert.deepStrictEqual(fileUploadModel.fileuploadAPI(), fileUploadMock.fileuploadAPI());
    });
  });
});