'use strict';
const Model = require('hof-govfrontend-v3').model;
const axios = require('axios').default;
const SubmitClaim = require('../../../../apps/student-claim/models/submit-claim');

const { chai, reqres, sinon, expect, assert } = require('../../../setup');

describe('Submit Claim Model', () => {
  let sandbox;
  let claim;
  const next = sinon.stub();

  beforeEach(function () {    
    sandbox = sinon.createSandbox();
  });

  afterEach(() => sandbox.restore());

  it('returns a promise', () => {
    const claim = sinon.stub();
    const submitClaim = new SubmitClaim();
    const response = submitClaim.submitClaim(claim,next);
    expect(response).to.be.an.instanceOf(Promise);
  });

  it('calls submitClaim with arguments', () => {
    const claim = sinon.stub();
    const submitClaim = new SubmitClaim();
    const response = submitClaim.submitClaim(claim,next);
  });


});