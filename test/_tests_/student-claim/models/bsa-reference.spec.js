'use strict';
const axios = require('axios').default;

const BsaRefModel = require('../../../../apps/student-claim/models/bsa-reference');
const ClaimUtility = require('../../../../apps/student-claim/utility/claim-utility');

const { reqres, sinon, expect, assert, httpMocks } = require('../../../setup');

const getBSARefTestConst = {
  getBSARef(req) {
    return 'BSA123123';
  },
};

describe("Get claim reference", function () {
  let sandbox;
  let req;
  let claim;
  const next = sinon.stub();

  beforeEach(function () {
    req = reqres.req();
    req.sessionModel.set = sinon.stub();
    claim = sinon.stub();
    sandbox = sinon.createSandbox();
  });

  afterEach(() => {
    afterEach(() => sandbox.restore());
  });

  it('returns a promise', () => {
    const bsaRefModel = new BsaRefModel();
    const response = bsaRefModel.getBSARef(claim, next);
    expect(response).to.be.an.instanceOf(Promise);
  });

  it("makes a call for creating claim reference", function () {
    sinon.stub(getBSARefTestConst, "getBSARef").callsFake(function fakeFn() {
      return "BSA123123";
    });
    assert.strictEqual(getBSARefTestConst.getBSARef(), "BSA123123");
  });

  it("makes a call to utility for bsaRefData", function () {
    
  });

  it('set from sessionModel not called', () => {
    const sessionModel = {}
    const bsaRefModel = new BsaRefModel();
    bsaRefModel.getBSARef(req, next);
    req.sessionModel.set.should.not.have.been.called;
  });

  it('should throw an error if can\'t create claim reference', () => {
    const bsaRefModel = new BsaRefModel();
    const stub = sinon.stub(bsaRefModel, 'getBSARef').rejects();
    return bsaRefModel.getBSARef(req, next).should.be.rejectedWith('')
      .then(() => {
        stub.restore();
      });
  });

  it('rejects if api call fails', function () {
    const bsaRefModel = new BsaRefModel();
    const error = new Error("Error creating claim reference");
    sandbox.stub(bsaRefModel, "getBSARef").throws(error);
    const call = function () {
      bsaRefModel.getBSARef();
    };
    expect(call).to.throw(Error);
  });

  it('throw error if claim creating call fails', function () {
    const bsaRefModel = new BsaRefModel();
    const error = new Error("Error creating claim reference");
    const call = sandbox.stub(bsaRefModel, "getBSARef").throws(error);
    expect(call).to.throw(Error);
  });

  it("should call axios post and setup session Model", () => {
    const expectedResponse = "BSA123123";
    const bsaRefModel = new BsaRefModel();

    const res = {
      bsaReference: sinon.spy()
    };
    const aStub = sinon.stub(axios, "post").resolves(expectedResponse);
    const req = httpMocks.createRequest({ method: "post", url: "/getBSARef" });
    bsaRefModel.getBSARef(req, res);
  });
});