const sessionModel = {
  'status' :'NEW',
  'source' :  "CLAIM_FORM",
  'personalDetails' :{
    'given-name': 'Personal',
    'family-name': 'Detail',
    'dependant-name': 'Dependant Details',
    'dependant-given-name': 'Dependant',
    'dependant-family-name': 'Details',
    'add-dependants-with-european-health-insurance-card-ehic': 'yes',
    'dependant-paid-work-uk': 'no',
    'dependant-living-in-uk': 'yes',
    'dependant-date-of-birth': '1998-02-12',
    'dependant-immigration-health-surcharge-number': 'IHS123456789',
    'dependant-share-code': 'A12 345 67G',
    'dependant-id': 'id1641296218741',
    'dependant-same-address-as-applicant': 'no',
    'dependant-enter-address': 'Mrs Smith 71<br>SOUTHAMPT Cherry Court<br>SOUTHAMPTON<br>UK<br>SO53 5PD<br>',
    'upload-dependant-european-health-insurance-card-ehic': 'dummy.pdf',
    'dependant-address-line-1': 'Mrs Smith 71',
    'dependant-address-line-2': 'SOUTHAMPT Cherry Court',
    'dependant-address-town': 'SOUTHAMPTON',
    'dependant-address-county': 'UK',
    'dependant-address-postcode': 'SO53 5PD'
  },
  'claim':{}
};

module.exports = {
  sessionModel
}