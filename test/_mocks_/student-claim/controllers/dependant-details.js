
const sessionModel = {
  'dependant-given-name': 'Dependant',
  'dependant-family-name': 'One',
  'add-dependants-with-european-health-insurance-card-ehic': 'yes',
  'dependant-paid-work-uk': 'no',
  'dependant-living-in-uk': 'yes',
  'dependant-date-of-birth': '1998-12-02',
  'dependant-immigration-health-surcharge-number': 'IHS223456789',
  'dependant-share-code': 'A1234567G',
  'dependant-id': 'abc123',
  'upload-dependant-european-health-insurance-card-ehic': [
    {
      srNo: 1,
      fileName: 'dummy.pdf',
      fileId: '1f4632f6-38ed-447e-a866-debb3efe8a9c'
    }
  ],
  'dependant-address-line-1': '123 Long Street',
  'dependant-address-line-2': 'Area',
  'dependant-address-town': 'London',
  'dependant-address-county': 'London',
  'dependant-address-postcode': 'SW1A 1AA'
};

const allDependants = [{
  'dependant-name': 'Dependant One',
  'dependant-enter-address': '123 Long Street, Area, London, SW1A 1AA',
  'upload-dependant-european-health-insurance-card-ehic':  [
    {
      srNo: 1,
      fileName: 'dummy.pdf',
      fileId: '1f4632f6-38ed-447e-a866-debb3efe8a9c'
    }
  ],
  ...sessionModel
}];

const displayDependants = [
  {
    'dependantIndex': 1,
    'dependantId': 'abc123',
    'dependantFields': [
      {
        "key": "add-dependants-with-european-health-insurance-card-ehic",
        'label': 'Translated Label',
        'url': false,
        'value': 'Tranformed Value'
      },
      {
        "key": "dependant-paid-work-uk",
        'label': 'Translated Label',
        'url': false,
        'value': 'Tranformed Value'
      },
      {
        "key": "dependant-living-in-uk",
        'label': 'Translated Label',
        'url': false,
        'value': 'Tranformed Value'
      },
      {
        "key": "dependant-name",
        'label': 'Translated Label',
        "url": "dependant-name",
        'value': 'Dependant One'
      },
      {
        "key": "dependant-date-of-birth",
        'label': 'Translated Label',
        "url": "dependant-date-of-birth",
        'value': 'Tranformed Value'
      },
      {
        "key": "dependant-immigration-health-surcharge-number",
        'label': 'Translated Label',
        "url": "dependant-immigration-health-surcharge-number",
        'value': 'IHS223456789'
      },
      {
        "key": "dependant-share-code",
        'label': 'Translated Label',
        "url": "dependant-share-code",
        'value': 'A1234567G'
      },
      {
        "key": "dependant-enter-address",
        'label': 'Translated Label',
        "url": "dependant-enter-address",
        'value': '123 Long Street, Area, London, SW1A 1AA'
      },
      {
        "key": "upload-dependant-european-health-insurance-card-ehic",
        'label': 'Translated Label',
        "url": "uploaded-dependant-files-european-health-insurance-card-ehic",
        'value': 'Tranformed Value'
      }
    ]
  }
];

const maxDependants = [
  {
    'dependant-id': 'abc001'
  },
  {
    'dependant-id': 'abc002'
  },
  {
    'dependant-id': 'abc003'
  },
  {
    'dependant-id': 'abc004'
  },
  {
    'dependant-id': 'abc005'
  },
  {
    'dependant-id': 'abc006'
  },
  {
    'dependant-id': 'abc007'
  },
  {
    'dependant-id': 'abc008'
  },
  {
    'dependant-id': 'abc009'
  },
  {
    'dependant-id': 'abc010'
  }
];

const addMoreDependantsError = { 'add-more-dependants': { type: 'maxDependants' } };

const dateInput = '2006-04-11';
const dateOutput = '11 April 2006';

const deleteDependant = 'Dependant One';

module.exports = {
  sessionModel,
  allDependants,
  displayDependants,
  deleteDependant,
  dateInput,
  dateOutput,
  maxDependants,
  addMoreDependantsError
}
