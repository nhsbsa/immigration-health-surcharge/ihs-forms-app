'use strict';
const dotenv = require('dotenv');
dotenv.config();
module.exports = {
  informationAboutEHIC: process.env.INFO_EHIC,
  startPageUrl: process.env.START_PAGE_URL,
  viewAndProveImmigrationStatus : process.env.IMMIGRATION_STATUS,
  completionPageFeedbackURL: process.env.COMPLETION_FEEDBACK_URL,
  contactUKLink: process.env.CONTACT_UK_LINK
};