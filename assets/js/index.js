/* eslint-disable */
'use strict';
var $ = require('jquery');
var dialogPolyfill = require('dialog-polyfill')
window.jQuery = $;
window.dialogPolyfill = dialogPolyfill;

var GOVUK = require('govuk-frontend')
var printEmail = require('./print-email');
var skipToMain = require('./skip-to-main');

GOVUK.initAll();
window.GOVUK = GOVUK;
var cookie = require('./govuk-cookies');
var cookieSettings = require('hof-govfrontend-v3/frontend/themes/gov-uk/client-js/index');

var sessionDialog = require('./session-dialog');
GOVUK.sessionDialog.init();
