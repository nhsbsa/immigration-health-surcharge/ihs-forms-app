var printEmailAddress = function () {
  var emailInput = document.getElementById("email-address");
  var emailShow = document.getElementById("email-show");
  if (emailInput) {
    document.getElementById("validEmail").style.display = 'none';

    emailInput.onkeyup = function (event) {
      var newText = event.target.value;
      emailShow.innerText = newText;
      if (event.target.value == '' || !event.target.value)
        document.getElementById("validEmail").style.display = 'none';
      else
        document.getElementById("validEmail").style.display = 'block';

    };
    if (emailInput.value) {
      emailShow.innerText = emailInput.value;
      document.getElementById("validEmail").style.display = 'block';
    }
  }
};
printEmailAddress();