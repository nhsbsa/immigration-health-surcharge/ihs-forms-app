var skipToMain = function () {
  var skipToMainLink = document.getElementById("skip-to-main");
  var firstControlId = skipToMainLink.hash.split('#')[1] ? skipToMainLink.hash.split('#')[1] : "main-content";
  if(firstControlId === "main-content"){
    skipToMainLink.setAttribute("href", "#main-content");
  }
  if(firstControlId) {
    skipToMainLink.onclick = function(e) {
      //here timeout added just to make this functionality asynchronous 
      //to focus on form as well as non form contents
      setTimeout(function () {
        var firstControl = document.getElementById(firstControlId);
        firstControl.focus();
      }(this), 10);    
    }
  }
  history.replaceState(null, null, ' ');
};
skipToMain();