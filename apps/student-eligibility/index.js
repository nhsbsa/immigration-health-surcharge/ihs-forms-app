'use strict';
const { informationAboutEHIC, startPageUrl } = require('./../../configRoutes');

const continueButtonId = 'continue'
module.exports = {
  name: 'student-eligibility',
  baseUrl: '/student-eligibility',
  confirmStep: '/eligibility-check-your-answers',
  steps: {
    '/start' :{
      controller: require('./../student-claim/controllers/clear-session')
    },
    '/paid-immigration-health-surcharge': {
      fields: ['paid-immigration-health-surcharge'],
      next: '/have-visa-on-after-2021',
      locals: {
        step: 'paid-immigration-health-surcharge',
        id: continueButtonId
      },
      backLink: false,
      forks: [{
        target: '/applicant-not-eligible',
        condition: {
          field: 'paid-immigration-health-surcharge',
          value: 'no'
        }
      }]
    },

    '/have-visa-on-after-2021': {
      fields: ['have-visa-on-after-2021'],
      next: '/have-european-health-insurance-card-ehic',
      locals: {
        step: 'have-visa-on-after-2021',
        id: continueButtonId
      },
      forks: [{
        target: '/applicant-not-eligible',
        condition: {
          field: 'have-visa-on-after-2021',
          value: 'no'
        }
      }]
    },
    '/have-european-health-insurance-card-ehic' : {
      fields: ['have-european-health-insurance-card-ehic'],
      next: '/full-time-education',
      locals: {
        step: 'have-european-health-insurance-card-ehic',
        informationAboutEHIC,
        id: continueButtonId
      },
      forks: [{
        target: '/applicant-not-eligible',
        condition: {
          field: 'have-european-health-insurance-card-ehic',
          value: 'no'
        }
      }]
    },
    '/full-time-education' : {
      fields: ['full-time-education'],
      next: '/paid-work-uk',
      locals: {
        step: 'full-time-education',
        id: continueButtonId
      },
      forks: [{
        target: '/applicant-not-eligible',
        condition: {
          field: 'full-time-education',
          value: 'no'
        }
      }]
    },
    '/paid-work-uk' : {
      fields: ['paid-work-uk'],
      next: '/living-in-uk',
      locals: {
        step: 'paid-work-uk',
        id: continueButtonId
      },
      forks: [{
        target: '/applicant-not-eligible',
        condition: {
          field: 'paid-work-uk',
          value: 'yes'
        }
      }]
    },
    '/living-in-uk' : {
      fields: ['living-in-uk'],
      next: '/eligibility-check-your-answers',
      locals: {
        step: 'living-in-uk',
        id: continueButtonId
      },
      forks: [{
        target: '/applicant-not-eligible',
        condition: {
          field: 'living-in-uk',
          value: 'no'
        }
      }]
    },
    '/eligibility-check-your-answers': {
      template: 'eligibility-check-your-answers',
      behaviours: [require('hof-govfrontend-v3').components.summary]
    },
    '/applicant-not-eligible':{
      template: 'applicant-not-eligible',
      locals: {
        backLink: '',
        startPageUrl
      }
    }
  }
};
