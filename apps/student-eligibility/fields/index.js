'use strict';
const hints = require('../translations/src/en/hints.json');

module.exports = {
  'paid-immigration-health-surcharge': {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'have-visa-on-after-2021':{
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'have-european-health-insurance-card-ehic' : {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    hintHTML: hints['have-european-health-insurance-card-ehic'].hint,
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'full-time-education' : {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    hintHTML: hints['full-time-education'].hint,
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'paid-work-uk' : {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    hintHTML: hints['paid-work-uk'].hint,
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'living-in-uk' : {
    mixin: 'radio-group',
    legend: {
      className: 'visuallyhidden'
    },
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  }
};
