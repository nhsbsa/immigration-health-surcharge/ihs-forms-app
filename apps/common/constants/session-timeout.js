const dotenv = require('dotenv');
dotenv.config();
// all timeout values in seconds
const sessionTimeOut = process.env.SESSION_TIMEOUT;
const sessionTimeOutWarning = process.env.SESSION_TIMEOUT_WARNING;
const finalSessionTimeOutWarning =  process.env.FINAL_SESSION_TIMEOUT_WARNING;

module.exports = {
  sessionTimeOut,
  sessionTimeOutWarning,
  finalSessionTimeOutWarning
};