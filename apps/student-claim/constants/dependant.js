'use strict';

const moreDependantClearSession = [
  'dependant-paid-work-uk',
  'dependant-living-in-uk',
  'dependant-given-name',
  'dependant-family-name',
  'dependant-date-of-birth',
  'dependant-immigration-health-surcharge-number',
  'dependant-share-code',
  'dependant-enter-address',
  'dependant-same-address-as-applicant',
  'dependant-address-line-1',
  'dependant-address-line-2',
  'dependant-address-town',
  'dependant-address-county',
  'dependant-address-postcode',
  'upload-dependant-european-health-insurance-card-ehic'
];

const notEligibleClearSession = [
  'dependant-paid-work-uk',
  'dependant-living-in-uk',
  'dependant-given-name',
  'dependant-family-name',
  'dependant-date-of-birth',
  'add-dependants-with-european-health-insurance-card-ehic',
  'dependant-immigration-health-surcharge-number',
  'dependant-share-code',
  'dependant-enter-address',
  'dependant-same-address-as-applicant',
  'dependant-address-line-1',
  'dependant-address-line-2',
  'dependant-address-town',
  'dependant-address-county',
  'dependant-address-postcode',
  'dependant-id',
  'upload-dependant-european-health-insurance-card-ehic'
];

const maxDependantCount = 10;


module.exports = {
  moreDependantClearSession,
  notEligibleClearSession,
  maxDependantCount
};