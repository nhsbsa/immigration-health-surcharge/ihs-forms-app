'use strict';
const path = require('path');
const date = require('hof-govfrontend-v3').components.date;
const hints = require('../translations/src/en/hints.json');
const nameValidator = [
  'required',
  { type: 'regex', arguments: /^[a-zA-Z\s\-']+$/ },
  { type: 'minlength', arguments: 2 },
  { type: 'maxlength', arguments: 50 }
]
const IHSValidator = [
  'required',
  { type: 'minlength', arguments: 12 },
  { type: 'regex', arguments: /^[Ii][Hh][Ss][Cc]{0,1}[0-9]{9,9}$/},
  { type: 'maxlength', arguments: 13 }
]
const AddressValidator = [
  'required',
  { type: 'regex', arguments: /^[a-zA-Z 0-9\s\-',]*$/ },
  { type: 'minlength', arguments: 2 },
  { type: 'maxlength', arguments: 35 }
]
const CountryValidator = [
  { type: 'regex', arguments: /^[a-zA-Z 0-9\s\-',]*$/ },
  { type: 'minlength', arguments: 2 },
  { type: 'maxlength', arguments: 35 }
]

module.exports = {
  'given-name': {
    mixin: 'input-text',
    validate: nameValidator,
    className: ['govuk-input']
  },
  'family-name': {
    mixin: 'input-text',
    validate: nameValidator,
    className: ['govuk-input']
  },
  'date-of-birth': date('date-of-birth', {
    controlType: 'date-input',
    validate: [
      'required',
    ],
    formGroup: {
      className: 'govuk-date-input__item'
    },
    controlsClass: {
      day: 'govuk-input govuk-date-input__input govuk-input--width-2',
      month: 'govuk-input govuk-date-input__input govuk-input--width-2',
      year: 'govuk-input govuk-date-input__input govuk-input--width-4'
    }
  }),
  'immigration-health-surcharge-number': {
    mixin: 'input-text',
    validate: IHSValidator,
    className: ['govuk-input', 'govuk-input--width-10']
  },
  'applicant-share-code': {
    mixin: 'input-text',
    validate: [
      'required',
      { type: 'regex', arguments: /^[a-zA-Z0-9]{3,3}\s?[a-zA-Z0-9]{3,3}\s?[a-zA-Z0-9]{3,3}$/ },
      { type: 'minlength', arguments: 9 },
      { type: 'maxlength', arguments: 12 }
    ],
    className: ['govuk-input', 'govuk-input--width-10']
  },
  'address-line-1': {
    mixin: 'input-text',
    validate:  AddressValidator,
    className: ['govuk-input']
  },
  'address-line-2': {
    mixin: 'input-text',
    validate: CountryValidator,
    className: ['govuk-input']
  },
  'address-town': {
    mixin: 'input-text',
    validate:  AddressValidator,
    className: ['govuk-input', 'govuk-!-width-two-thirds']
  },
  'address-county': {
    mixin: 'input-text',
    validate: CountryValidator,
    className: ['govuk-input', 'govuk-!-width-two-thirds']
  },
  'address-postcode': {
    mixin: 'input-text',
    validate: [
      'required',
      'postcode'
    ],
    className: ['govuk-input', 'govuk-input--width-10']
  },
  'email-address': {
    mixin: 'input-text',
    validate: [
      'required',
      'email',
      { type: 'maxlength', arguments: 50 }
    ],
    className: ['govuk-input']
  },
  'telephone-number': {
    mixin: 'input-phone',
    validate: [
      { type: 'regex', arguments: /^(^$|[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[\. -/0-9]*)$/ },
      { type: 'maxlength', arguments: 15 },
      { type: 'minlength', arguments: 11 }
    ],
    className: ['govuk-input', 'govuk-input--width-20'],
    labelClassName: ['govuk-visually-hidden']
  },
  'upload-european-health-insurance-card-ehic': {
    mixin: 'input-file',
    className: ['govuk-file-upload'],
    labelClassName: ['govuk-label govuk-label--m']
  },
  'upload-evidence': {
    mixin: 'input-file',
    className: ['govuk-file-upload'],
    labelClassName: ['govuk-label govuk-label--m']
  },
  'add-dependants-with-european-health-insurance-card-ehic': {
    mixin: 'radio-group',
    includeInSummary: false,
    options: ['yes', 'no'],
    hintHTML: hints['add-dependants-with-european-health-insurance-card-ehic'].hint,
    validate: 'required',
  },
  'add-more-dependants': {
    mixin: 'radio-group',
    includeInSummary: false,
    legend: {
      className: 'govuk-visually-hidden'
    },
    options: ['yes', 'no'],
    validate: 'required',
  },
  'dependant-paid-work-uk': {
    mixin: 'radio-group',
    hintHTML: hints['dependant-paid-work-uk'].hint,
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'dependant-living-in-uk': {
    mixin: 'radio-group',
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'dependant-given-name': {
    mixin: 'input-text',
    validate: nameValidator,
    className: ['govuk-input']
  },
  'dependant-family-name': {
    mixin: 'input-text',
    validate: nameValidator,
    className: ['govuk-input'],
    legend: {
      className: 'govuk-label'
    }
  },
  'dependant-date-of-birth': date('dependant-date-of-birth', {
    controlType: 'date-input',
    validate: [
      'required',
      'date',
      'before'
    ],
    formGroup: {
      className: 'govuk-date-input__item'
    },
    controlsClass: {
      day: 'govuk-input govuk-date-input__input govuk-input--width-2',
      month: 'govuk-input govuk-date-input__input govuk-input--width-2',
      year: 'govuk-input govuk-date-input__input govuk-input--width-4'
    }
  }),
  'dependant-immigration-health-surcharge-number': {
    mixin: 'input-text',
    validate: IHSValidator,
    className: ['govuk-input', 'govuk-input--width-10']
  },
  'dependant-share-code': {
    mixin: 'input-text',
    validate: [
      'required',
      { type: 'regex', arguments: /^[a-zA-Z0-9]{3,3}\s?[a-zA-Z0-9]{3,3}\s?[a-zA-Z0-9]{3,3}$/ },
      { type: 'minlength', arguments: 9 },
      { type: 'maxlength', arguments: 12 }
    ],
    className: ['govuk-input', 'govuk-input--width-10']
  },
  'dependant-same-address-as-applicant': {
    mixin: 'radio-group',
    includeInSummary: false,
    options: ['yes', 'no'],
    validate: 'required'
  },
  'dependant-address-line-1': {
    mixin: 'input-text',
    validate: AddressValidator,
    className: ['govuk-input']
  },
  'dependant-address-line-2': {
    mixin: 'input-text',
    validate: CountryValidator,
    className: ['govuk-input']
  },
  'dependant-address-town': {
    mixin: 'input-text',
    validate: AddressValidator,
    className: ['govuk-input', 'govuk-!-width-two-thirds']
  },
  'dependant-address-county': {
    mixin: 'input-text',
    validate: CountryValidator,
    className: ['govuk-input', 'govuk-!-width-two-thirds']
  },
  'dependant-address-postcode': {
    mixin: 'input-text',
    validate: [
      'required',
      'postcode'
    ],
    className: ['govuk-input', 'govuk-input--width-10']
  },
  'upload-dependant-european-health-insurance-card-ehic': {
    mixin: 'input-file',
    className: ['govuk-file-upload'],
    labelClassName: ['govuk-label govuk-label--m']
  }
};

