const moment = require('moment');
const nodeBase64 = require('nodejs-base64-converter');

module.exports = class ClaimUtility {  

  static bsaRefData(claim) {
    return {
      sessionId : claim.sessionId,
      cohort  : claim.cohort,
      status     : 'DRAFT'
    };
  }

  static fileData(file){
    return {
      "fileName": file.name,
      "contentType":  file.mimetype,
      "data":nodeBase64.encode(file.data)
    };
  }

  static generateDependantClaimDetails(allDependants) {
    const dependantsClaimDetails = allDependants.map(dependant => {
      return {
        "givenName": dependant['dependant-given-name'],
        "familyName": dependant['dependant-family-name'],
        "ihsReference": dependant['dependant-immigration-health-surcharge-number'],
        "dateOfBirth": dependant['dependant-date-of-birth'],
        "visaShareCode": dependant['dependant-share-code'],
        "address": this.generateDependantAddress(dependant)
      };
    })
    return dependantsClaimDetails;
  }
  static generateDependantAddress(dependant){
    return { 
      "addressLine1": dependant['dependant-address-line-1'],
      "addressLine2": dependant['dependant-address-line-2'],
      "postTown": dependant['dependant-address-town'],
      "county": dependant['dependant-address-county'],
      "postcode": dependant['dependant-address-postcode']
    }
  }

  static generateClaimDetails(claim, personalDetails, dependantClaimDetails) {    
      claim.status ='NEW';
      claim.source =  "CLAIM_FORM";
      claim.givenName=  personalDetails['given-name'];
      claim.familyName= personalDetails['family-name'];
      claim.dateOfBirth= personalDetails['date-of-birth'],
      claim.emailAddress= personalDetails['email-address'],
      claim.telephone= personalDetails['telephone-number'],
      claim.address = this.generateAddress(personalDetails),
      claim.contactMethodPreference= "EMAIL",
      claim.languagePreference= "EN",
      claim.ihsReference= personalDetails['immigration-health-surcharge-number'],
      claim.visaShareCode= personalDetails['applicant-share-code'],
      claim.declarationDate= moment().format('YYYY-MM-DD HH:mm:ss'),
      claim.createdBy= "UNAUTHENTICATED",
      claim.channel= "ONLINE",
      claim.cohort= "STUDENT",
      claim.dependants= dependantClaimDetails   
  }
  
  static generateAddress(personalDetails){
    return {
      "addressLine1": personalDetails['address-line-1'],
      "addressLine2": personalDetails['address-line-2'],
      "postTown": personalDetails['address-town'],
      "county": personalDetails['address-county'],
      "postcode": personalDetails['address-postcode']
    }
  }

  static updateClaimFromSession(model){
    let claim = model['claim']; 
    const allDependants = model['all-dependants'] ? model['all-dependants'] : [];
    const personalDetails = model['personalDetails'];
    const dependantClaimDetails = this.generateDependantClaimDetails(allDependants);
    let temp =  this.generateClaimDetails(claim,personalDetails, dependantClaimDetails  );
    return claim;
  }

}