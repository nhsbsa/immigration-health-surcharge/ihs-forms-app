module.exports = class PersonalDetailsModel {
  constructor(model) {
    this['name'] = [model['given-name'], ' ', model['family-name']].join('');
    this['given-name'] = model['given-name'];
    this['family-name'] = model['family-name'];
    this['date-of-birth'] = model['date-of-birth'];
    this['immigration-health-surcharge-number'] = model['immigration-health-surcharge-number'].toUpperCase();
    this['applicant-share-code'] = model['applicant-share-code'].toUpperCase();
    this['telephone-number'] = model['telephone-number'];
    this['email-address'] = model['email-address'];
    this['enter-address'] = this.generateAddress(model);
    this['applicant-file-upload'] = [model['ehicFile'], model['studyLetter']];
    this['address-line-1'] = model['address-line-1'];
    this['address-line-2'] = model['address-line-2'];
    this['address-town'] = model['address-town'];
    this['address-county'] = model['address-county'];
    this['address-postcode'] = model['address-postcode'];
  }

  generateAddress(model) {
    const addressLine1 = model['address-line-1'] ? (model['address-line-1'] + '<br>') : '';
    const addressLine2 = model['address-line-2'] ? (model['address-line-2'] + '<br>') : '';
    const addressTown = model['address-town'] ? (model['address-town'] + '<br>') : '';
    const addressCounty = model['address-county'] ? (model['address-county'] + '<br>') : '';
    const addressPostcode = model['address-postcode'] ? (model['address-postcode'] + '<br>') : '';

    return addressLine1 + addressLine2 + addressTown + addressCounty + addressPostcode;
  }
}