'use strict';
const Model = require('hof-govfrontend-v3').model;
const axios = require('axios').default;
const ClaimUtility = require('../utility/claim-utility');

module.exports = class BsaRefService extends Model {
  async getBSARef (claim,next){
    try{
      const headers = { "Content-type": "application/json", "x-api-key": process.env.IHS_API_KEY };      
      return await axios.post(process.env.BSA_URL, ClaimUtility.bsaRefData(claim), { headers }).then(bsaRef => {
        return bsaRef.data.bsaReference;   
      }).catch(error => {  
        if((error.response.status === 409) && (error.response.data.message === 'Request has been processed previously')){
          return error.response.data.bsaReference; 
        }
        console.error('Error creating claim reference!' + error);
        next(new Error(error.body));
     });      
    }catch (error) {
      console.error('Error creating claim reference!!' + error);
      next(new Error(error.body));
    }
  }
}