'use strict';
const Model = require('hof-govfrontend-v3').model;
const axios = require('axios').default;

module.exports = class FileUploadModel extends Model {
  async fileuploadAPI(claim, file, next) {
    try {
      console.log('Uploading the Evidence for :' + claim.bsaRef);
      const url = process.env.BSA_URL + '/' + claim.bsaRef + '/evidence';
      const headers = { "Content-type": "application/json", "x-api-key": process.env.IHS_API_KEY };
      const fileServiceRes = await axios.post(url, file, { headers });
      return fileServiceRes;
    } catch (error) {
      console.error('Error uploading file evidence for claim : ' + claim.bsaRef + ' ' + error);
    }
  }

  async fileDeleteApi(claim, fileId) {
    try {
      console.log('Deleting the Evidence for :' + claim.bsaRef);
      const url = process.env.BSA_URL + '/' + claim.bsaRef + '/evidence/' + fileId;
      const headers = { "x-api-key": process.env.IHS_API_KEY };
      const fileServiceRes =  await axios.delete(url, { headers });
      return fileServiceRes;
    } catch (error) {
      console.error('Error deleting file evidence for claim : ' + claim.bsaRef + ' ' + error);
    }
  }

};