const axios = require('axios').default;

module.exports = class SubmitClaimApiReferanceModel {
   async submitClaim(claim, next) { 
    console.log("Submitting claim for "+ claim.bsaRef);     
    const headers = { "Content-type": "application/json", "x-api-key": process.env.IHS_API_KEY };
    try {
      const url = process.env.BSA_URL + '/' + claim.bsaRef; 
      const referenceNumber = await axios.post(url, claim, { headers }); 
      return  referenceNumber.data.bsaReference;  
    } catch (error) {
      console.error('Error in submitting the claim ' + error);
      next(new Error(error.body));
    }
  }
}