module.exports = class DependantModel {
  constructor(model,takeApplicantAddress = true) {
    this['dependant-name'] = [model['dependant-given-name'], ' ', model['dependant-family-name']].join('');
    this['dependant-given-name'] = model['dependant-given-name'];
    this['dependant-family-name'] = model['dependant-family-name'];
    this['add-dependants-with-european-health-insurance-card-ehic'] = model['add-dependants-with-european-health-insurance-card-ehic'];
    this['dependant-paid-work-uk'] = model['dependant-paid-work-uk'];
    this['dependant-living-in-uk'] = model['dependant-living-in-uk'];
    this['dependant-date-of-birth'] = model['dependant-date-of-birth'];
    this['dependant-immigration-health-surcharge-number'] = model['dependant-immigration-health-surcharge-number'].toUpperCase();
    this['dependant-share-code'] = model['dependant-share-code'].toUpperCase();
    this['dependant-id'] = model['dependant-id'];
    this['dependant-same-address-as-applicant'] = model['dependant-same-address-as-applicant'];
    this['dependant-enter-address'] = this.generateAddress(model,takeApplicantAddress);
    this['upload-dependant-european-health-insurance-card-ehic'] = model['dependant-ehic'];
    this['dependant-address-line-1'] = model['dependant-address-line-1'] ? model['dependant-address-line-1'] : model['address-line-1'];
    this['dependant-address-line-2'] = model['dependant-address-line-1'] ? model['dependant-address-line-2'] : model['address-line-2'];
    this['dependant-address-town'] = model['dependant-address-town'] ? model['dependant-address-town'] : model['address-town'];
    this['dependant-address-county'] = model['dependant-address-county'] ? model['dependant-address-county'] : model['address-county'];
    this['dependant-address-postcode'] = model['dependant-address-postcode'] ? model['dependant-address-postcode'] : model['address-postcode'];
  }

  editDependant(model,takeApplicantAddress){
    const editDep = model;
    editDep['dependant-name'] = [model['dependant-given-name'], ' ', model['dependant-family-name']].join('');
    editDep['dependant-paid-work-uk'] = model['dependant-paid-work-uk'];
    editDep['dependant-living-in-uk'] = model['dependant-living-in-uk'];    
    editDep['dependant-share-code'] = model['dependant-share-code'].toUpperCase();    
    editDep['dependant-same-address-as-applicant'] = model['dependant-same-address-as-applicant'];
    editDep['dependant-enter-address'] = this.generateAddress(model,takeApplicantAddress);
    editDep['upload-dependant-european-health-insurance-card-ehic'] = model['upload-dependant-european-health-insurance-card-ehic'];
    editDep['dependant-address-line-1'] = model['dependant-address-line-1'] ? model['dependant-address-line-1'] : model['address-line-1'];
    editDep['dependant-address-line-2'] = model['dependant-address-line-1'] ? model['dependant-address-line-2'] : model['address-line-2'];
    editDep['dependant-address-town'] = model['dependant-address-town'] ? model['dependant-address-town'] : model['address-town'];
    editDep['dependant-address-county'] = model['dependant-address-county'] ? model['dependant-address-county'] : model['address-county'];
    editDep['dependant-address-postcode'] = model['dependant-address-postcode'] ? model['dependant-address-postcode'] : model['address-postcode'];
    
   return editDep;
  }

  generateAddress(model,takeApplicantAddress = true) {
    const keyPrefix = (model['dependant-same-address-as-applicant'] == 'yes' && takeApplicantAddress == true) ? '' : 'dependant-';
    const addressLine1 = model[keyPrefix + 'address-line-1'] ? (model[keyPrefix + 'address-line-1'] + '<br>') : '';
    const addressLine2 = model[keyPrefix + 'address-line-2'] ? (model[keyPrefix + 'address-line-2'] + '<br>') : '';
    const addressTown = model[keyPrefix + 'address-town'] ? (model[keyPrefix + 'address-town'] + '<br>') : '';
    const addressCounty = model[keyPrefix + 'address-county'] ? (model[keyPrefix + 'address-county'] + '<br>') : '';
    const addressPostcode = model[keyPrefix + 'address-postcode'] ? (model[keyPrefix + 'address-postcode'] + '<br>') : '';
    return addressLine1 + addressLine2 + addressTown + addressCounty + addressPostcode;
  }
}