module.exports = class AddressModel {
  generateAddress(model,takeApplicantAddress = true) {    
    const keyPrefix = '';
    const addressLine1 = model[keyPrefix + 'address-line-1'] ? (model[keyPrefix + 'address-line-1'] + '<br>') : '';
    const addressLine2 = model[keyPrefix + 'address-line-2'] ? (model[keyPrefix + 'address-line-2'] + '<br>') : '';
    const addressTown = model[keyPrefix + 'address-town'] ? (model[keyPrefix + 'address-town'] + '<br>') : '';
    const addressCounty = model[keyPrefix + 'address-county'] ? (model[keyPrefix + 'address-county'] + '<br>') : '';
    const addressPostcode = model[keyPrefix + 'address-postcode'] ? (model[keyPrefix + 'address-postcode'] + '<br>') : '';
    return addressLine1 + addressLine2 + addressTown + addressCounty + addressPostcode;   
  }

  copyDepedantAddress(req,dependantIndex,allDependants){
    allDependants[dependantIndex]['dependant-address-line-1'] = req.form.values['address-line-1'];
    allDependants[dependantIndex]['dependant-address-line-2'] = req.form.values['address-line-2'];
    allDependants[dependantIndex]['dependant-address-town'] = req.form.values['address-town'];
    allDependants[dependantIndex]['dependant-address-county'] = req.form.values['address-county'];
    allDependants[dependantIndex]['dependant-address-postcode'] = req.form.values['address-postcode'];
    allDependants[dependantIndex]['dependant-enter-address'] = this.generateAddress(req.form.values);
    return allDependants;
  }  
}