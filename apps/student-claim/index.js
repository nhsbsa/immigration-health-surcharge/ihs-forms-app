'use strict';
const { viewAndProveImmigrationStatus, startPageUrl, completionPageFeedbackURL  ,contactUKLink} = require('./../../configRoutes');
const continueButtonId = 'continue'
module.exports = {
  name: 'student-claim',
  baseUrl: '/student-claim',
  confirmStep: '/check-your-answers',
  steps: {
    '/start' :{
      controller: require('./controllers/clear-session')
    },
    '/name': {
      fields: ['given-name', 'family-name'],
      next: '/date-of-birth',
      locals: {
        step: 'name',
        labelClassName: 'govuk-input',
        id: continueButtonId
      },
      backLink: false
    },
    '/date-of-birth': {
      fields: ['date-of-birth'],
      next: '/immigration-health-surcharge-number',
      controller: require('./controllers/validate-date-of-birth'),
      locals: {
        step: 'date-of-birth',
        labelClassName: 'govuk-input',
        id: continueButtonId
      }
    },
    '/immigration-health-surcharge-number' :{
      fields: ['immigration-health-surcharge-number'],
      next: '/applicant-share-code',
      locals: {
        step: 'immigration-health-surcharge-number',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId,
        contactUKLink
      }
    },
    '/applicant-share-code' :{
      fields: ['applicant-share-code'],
      next: '/enter-address',
      locals: {
        step: 'applicant-share-code',
        viewAndProveImmigrationStatus,
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      }
    },
    '/enter-address':{
      fields: ['address-line-1', 'address-line-2', 'address-town', 'address-county', 'address-postcode'],
      next: '/email-address',
      controller: require('./controllers/enter-address'),
      locals: {
        step: 'enter-address',
        id: continueButtonId
      }
    },
    '/email-address':{
      fields: ['email-address'],
      next: '/telephone-number',
      locals: {
        step: 'email-address',
        id: continueButtonId
      }
    },
    '/telephone-number':{
      fields: ['telephone-number'],
      next: '/upload-european-health-insurance-card-ehic',
      controller: require('./controllers/get-bsa-referance'),
      locals: {
        step: 'telephone-number',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      }
    },
    '/upload-european-health-insurance-card-ehic':{
      fields: ['upload-european-health-insurance-card-ehic'],
      next: '/uploaded-files-european-health-insurance-card-ehic',
      behaviours: require('./behaviours/upload-file'),
      locals: {
        step: 'upload-european-health-insurance-card-ehic',
        fileNameKey:'ehicFile',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      },
    },
    '/uploaded-files-european-health-insurance-card-ehic' : {
      fields: ['uploaded-files-european-health-insurance-card-ehic'],
      controller: require('./controllers/get-file-values'),
      next: '/upload-evidence',
      locals: {
        step: 'uploaded-files-european-health-insurance-card-ehic',
        id: continueButtonId
      },
      backLink: false
    },
    '/upload-evidence':{
      fields: ['upload-evidence'],
      next: '/uploaded-files-evidence',
      behaviours: require('./behaviours/upload-file'),
      locals: {
        step: 'upload-evidence',
        fileNameKey:'studyLetter',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      }
    },
    '/uploaded-files-evidence' : {
      fields: ['uploaded-files-evidence'],
      next: '/add-dependants-with-european-health-insurance-card-ehic',
      controller: require('./controllers/get-file-values'),
      locals: {
        step: 'uploaded-files-evidence',
        id: continueButtonId
      },
      backLink: false
    },
    '/add-dependants-with-european-health-insurance-card-ehic':{
      fields: ['add-dependants-with-european-health-insurance-card-ehic'],
      controller: require('./controllers/add-dependant'),
      next: '/check-your-answers',
      forks: [{
        isLoop: true,
        target: '/dependant-paid-work-uk',
        condition: {
          field: 'add-dependants-with-european-health-insurance-card-ehic',
          value: 'yes'
        }
      }],
      locals: {
        step: 'add-dependants-with-european-health-insurance-card-ehic',
        id: continueButtonId
      }
    },
    '/dependant-paid-work-uk' : {
      fields: ['dependant-paid-work-uk'],
      next: '/dependant-living-in-uk',
      controller: require('./controllers/fetch-dependant'),
      backLink: 'add-dependants-with-european-health-insurance-card-ehic',
      locals: {
        step: 'dependant-paid-work-uk',
        id: continueButtonId
      },
      forks: [{
        target: '/dependant-not-eligible',
        condition: {
          field: 'dependant-paid-work-uk',
          value: 'yes'
        }
      }],
      continueOnEdit: true
    },
    '/dependant-living-in-uk' : {
      fields: ['dependant-living-in-uk'],
      next: '/dependant-name',
      controller: require('./controllers/fetch-dependant'),
      locals: {
        step: 'dependant-living-in-uk',
        id: continueButtonId
      },
      forks: [{
        target: '/dependant-not-eligible',
        condition: {
          field: 'dependant-living-in-uk',
          value: 'no'
        }
      }]
    },
    '/dependant-name': {
      fields: ['dependant-given-name', 'dependant-family-name'],
      next: '/dependant-date-of-birth',
      controller: require('./controllers/fetch-dependant'),
      locals: {
        step: 'dependant-name',
        id: continueButtonId
      }
    },
    '/dependant-date-of-birth': {
      fields: ['dependant-date-of-birth'],
      controller: require('./controllers/fetch-dependant'),
      next: '/dependant-immigration-health-surcharge-number',
      locals: {
        step: 'dependant-date-of-birth',
        id: continueButtonId
      }
    },
    '/dependant-immigration-health-surcharge-number' :{
      fields: ['dependant-immigration-health-surcharge-number'],
      controller: require('./controllers/fetch-dependant'),
      template: 'immigration-health-surcharge-number',
      next: '/dependant-share-code',
      locals: {
        step: 'dependant-immigration-health-surcharge-number',
        id: continueButtonId,
        contactUKLink
      }
    },
    '/dependant-share-code' :{
      fields: ['dependant-share-code'],
      template: 'applicant-share-code',
      next: '/dependant-same-address-as-applicant',
      controller: require('./controllers/fetch-dependant'),
      locals: {
        step: 'dependant-share-code',
        viewAndProveImmigrationStatus,
        id: continueButtonId
      }
    },
    '/dependant-same-address-as-applicant' :{
      fields: ['dependant-same-address-as-applicant'],
      controller: require('./controllers/fetch-dependant'),
      template: 'dependant-paid-work-uk',
      next: '/upload-dependant-european-health-insurance-card-ehic',
      locals: {
        step: 'dependant-same-address-as-applicant',
        viewAndProveImmigrationStatus,
        id: continueButtonId
      },
      forks: [{
        target: '/dependant-enter-address',
        condition: {
          field: 'dependant-same-address-as-applicant',
          value: 'no'
        }
      }]
    },
    '/dependant-enter-address':{
      fields: ['dependant-address-line-1', 'dependant-address-line-2', 'dependant-address-town', 'dependant-address-county', 'dependant-address-postcode'],
      template: 'enter-address',
      next: '/upload-dependant-european-health-insurance-card-ehic',
      controller: require('./controllers/fetch-dependant'),
      locals: {
        step: 'dependant-enter-address',
        id: continueButtonId
      }
    },
    '/upload-dependant-european-health-insurance-card-ehic':{
      fields: ['upload-dependant-european-health-insurance-card-ehic'],
      template: 'upload-european-health-insurance-card-ehic',
      next: '/uploaded-dependant-files-european-health-insurance-card-ehic',
      behaviours: require('./behaviours/upload-file'),
      locals: {
        step: 'upload-dependant-european-health-insurance-card-ehic',
        fileNameKey:'dependant-ehic',
        labelClassName: 'govuk-input govuk-input--width-10',
        id: continueButtonId
      },
    },
    '/uploaded-dependant-files-european-health-insurance-card-ehic' : {
      fields: ['uploaded-dependant-files-european-health-insurance-card-ehic'],
      template: 'uploaded-files-european-health-insurance-card-ehic',
      controller: require('./controllers/get-dependant-file-name'),
      next: '/check-dependant-answers',
      locals: {
        step: 'uploaded-dependant-files-european-health-insurance-card-ehic',
        id: continueButtonId
      },
      backLink: false
    },
    '/check-dependant-answers' :{
      fields: ['add-more-dependants'],
      next: '/check-your-answers',
      controller: require('./controllers/dependant-details'),
      locals: {
        step: 'check-dependant-answers',
        id: continueButtonId
      },
      backLink: false
    },
    '/check-your-answers' :{
      controller: require('./controllers/summary-details'),
      next: '/declaration',
      locals: {
        step: 'check-your-answers',
        id: continueButtonId
      }
    },
    '/delete-dependant' :{
      controller: require('./controllers/delete-dependant'),
      backLink: false
    },
    '/delete-files' :{
      controller: require('./controllers/delete-files'),
      backLink: false
    },
    '/edit-dependant' :{
      controller: require('./controllers/edit-dependant'),
      backLink: false
    },
    '/dependant-not-eligible':{
      template: 'dependant-not-eligible',
      next: '/add-dependants-with-european-health-insurance-card-ehic',
      controller: require('./controllers/dependant-not-eligible'),
      locals: {
        startPageUrl
      },
      backLink: false
    },
    '/declaration':{
      template: 'declaration',
      next: '/application-complete',
      controller: require('./controllers/submit-claim')      
    },
    '/application-complete':{
      template: 'application-complete',
      controller: require('./controllers/get-claim-reference-number'),
      locals: {
        feedbackUrl:'', 
        completionPageFeedbackURL
      },
      backLink: false
    }
  }
};