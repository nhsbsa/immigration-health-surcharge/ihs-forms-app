const Controller = require('hof-govfrontend-v3').controller;
const AddressModel = require('../models/address');

module.exports = class DependantAddController extends Controller {

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);  
    return locals;
  }

  process(req, res, next) {
    const addressModel = new AddressModel();
    let allDependants = req.sessionModel.get('all-dependants');
     if(allDependants && allDependants != null){
      allDependants.forEach((dependant, index) => {
        if(dependant['dependant-same-address-as-applicant'] === 'yes'){
          allDependants = addressModel.copyDepedantAddress(req,index,allDependants);
        }     
      });
      req.sessionModel.set('all-dependants', allDependants);
    }    
    next();
  }
};