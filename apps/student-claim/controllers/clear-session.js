'use strict';
const Controller = require('hof-govfrontend-v3').controller;
module.exports = class clearDataSession extends Controller {
  getValues(req, res) {
    const sessionCleared = req.query.sessionCleared;
    if (!sessionCleared) {
      req.session.destroy()
      const nextNav = '/start?sessionCleared=true';
      return res.redirect(`${req.baseUrl}${nextNav}`);
    } else {
      const nextNav = (req.baseUrl === '/student-eligibility') ? '/paid-immigration-health-surcharge' : '/name';
      return res.redirect(`${req.baseUrl}${nextNav}`);
    }
  }
  
};
