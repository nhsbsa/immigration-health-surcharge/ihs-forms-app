const Controller = require('hof-govfrontend-v3').controller;
module.exports = class ClaimReferenceNumberController extends Controller {
 
  getValues(req, res, callback) {
    const field = this.options.locals.field;    
    super.getValues(req, res, callback);
  }

  locals(req, res ,callback) {
    const locals = super.locals(req, res,callback);
    locals.claimReferenceNumber = req.sessionModel.get('claim-reference-number');
    return locals;
  }
};