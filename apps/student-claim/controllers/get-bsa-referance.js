const Controller = require('hof-govfrontend-v3').controller;
const BsaRefModel = require('../models/bsa-reference');
const Claim = require('../models/claim');
const path = require('path');

module.exports = class BsaRefController extends Controller {
  async process(req, res, next) {
    try {      
      let claim = req.sessionModel.get('claim') ? req.sessionModel.get('claim') : [];
      if(!claim.bsaRef || claim.bsaRef == ''){
        claim = await this.createNewDraftClaim(req.sessionID,next);
        req.sessionModel.set('claim', claim);
      }
      req.form.values[ 'telephone-number'] =  req.form.values[ 'telephone-number'].replace(/\(/g,"").replace(/\)/g,""); 
      next();
    } catch {
      const err = new this.ValidationError('telephone-number', {
        type: 'error'
      }, req, res);
      const errorObject = {};
      errorObject['telephone-number'] = err;
      return next(errorObject);
    }
  }

  async createNewDraftClaim(sessionId,next){
    const claimObj = new Claim();
    claimObj.sessionId = sessionId;
    claimObj.cohort= "STUDENT";
    const bsaRefModel = new BsaRefModel();    
    claimObj.bsaRef = await bsaRefModel.getBSARef(claimObj, next);
    console.log("Created new BSARef "+ claimObj.bsaRef);
    return claimObj;
  }
};