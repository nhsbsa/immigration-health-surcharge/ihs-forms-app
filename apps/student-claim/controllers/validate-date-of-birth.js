'use strict';
const Controller = require('hof-govfrontend-v3').controller;
const moment = require('moment');
const dateFormat = 'YYYY-MM-DD';

module.exports = class clearDataSession extends Controller {
    async process(req, res, next) {
        let validationDate = req.sessionModel.get('validation-date');

        let currentDate = (typeof validationDate === 'undefined') ? moment(new Date()).format(dateFormat) : validationDate;

        let validate = this.checkValidation(req.form.values['date-of-birth'], req, res);
        let before = this.beforeCheck(req.form.values['date-of-birth'], currentDate, [16, 'years'], req, res);

        if (!validate.status) {
            return next(validate.errorObject);
        } else if (!before.status) {
            return next(before.errorObject);
        } else {
            return next();
        }
    }

    flashError(req, res, validationType) {
        return {
            'date-of-birth': new this.ValidationError('date-of-birth', {
                type: validationType
            }, req, res)
        };
    }

    checkValidation(formValue, req, res) {

        if (formValue === '')
            return { status: false, errorObject: this.flashError(req, res, 'required') };

        if ((/^-\d{2}$/).test(formValue) || (/^-\d{2}-$/).test(formValue) || (/^\d{1,4}-$/).test(formValue))
            return { status: false, errorObject: this.flashError(req, res, 'missingField') };

        if ((/^\d{1,4}\-\d{2}\-+$/).test(formValue))
            return { status: false, errorObject: this.flashError(req, res, 'missing-day') };

        if ((/^\d{1,4}\-\d{2}$/).test(formValue))
            return { status: false, errorObject: this.flashError(req, res, 'missing-month') };

        if ((/^-\d{2}\-\d{2}$/).test(formValue))
            return { status: false, errorObject: this.flashError(req, res, 'missing-year') };

        let dateArray = formValue.split('-');
        if (dateArray.reduce((errorCount, dateValue) => (!(/^[0-9]+$/).test(dateValue)) ? errorCount + 1 : errorCount, 0) >= 2)
            return { status: false, errorObject: this.flashError(req, res, 'numberOnly') };

        if (!(/^[0-9]+$/).test(dateArray[0]))
            return { status: false, errorObject: this.flashError(req, res, 'number-year') };

        if (!(/^[0-9]+$/).test(dateArray[1]))
            return { status: false, errorObject: this.flashError(req, res, 'number-month') };

        if (!(/^[0-9]+$/).test(dateArray[2]))
            return { status: false, errorObject: this.flashError(req, res, 'number-day') };

        if (!moment(formValue, dateFormat).isValid())
            return { status: false, errorObject: this.flashError(req, res, 'impossibleDate') };

        if (!(/\d{4}\-\d{2}\-\d{2}/).test(formValue) && moment(formValue, dateFormat).isValid())
            return { status: false, errorObject: this.flashError(req, res, 'date') };

        return { status: true, errorObject: {} };
    }

    beforeCheck(formValue, validationDate, argumentsList, req, res) {

        let valueDate = moment(formValue, dateFormat).add(parseInt(argumentsList[0]), argumentsList[1]);

        if (moment(validationDate, dateFormat).isBefore(valueDate))
            return { status: false, errorObject: this.flashError(req, res, 'before') }
        else
            return { status: true, errorObject: {} };
    }
}