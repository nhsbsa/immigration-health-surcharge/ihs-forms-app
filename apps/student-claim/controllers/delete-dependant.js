const Controller = require('hof-govfrontend-v3').controller;
const FileUploadModel = require('../models/file-upload');
const fileUploadModel = new FileUploadModel();

module.exports = class DependantDeleteController extends Controller {

 async get(req, res, next) {
    const dependantId = req.query.dependantId;
    if (dependantId) {
      await this.deleteDependant(req, res, dependantId) ;
    }
    next();
  }

  async deleteDependant(req, res, dependantId) {
    const allDependants = req.sessionModel.get('all-dependants');
    try {      
      const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === dependantId)
      if (dependantIndex !== -1) {
        let fileId = allDependants[dependantIndex]['upload-dependant-european-health-insurance-card-ehic'][0]['fileId'];
       
        const deletedDependant = (dependantIndex + 1) + ' ' + allDependants[dependantIndex]['dependant-name'];
        allDependants.splice(dependantIndex, 1);
        req.sessionModel.set('all-dependants', allDependants);
        req.sessionModel.set('deletedDependant', deletedDependant);
        req.sessionModel.unset('dependant-id');
        let fileUploadRequestObj = await fileUploadModel.fileDeleteApi(req.sessionModel.get('claim'), fileId);
        const nextNav = allDependants.length === 0 ? '/add-dependants-with-european-health-insurance-card-ehic' : '/check-dependant-answers';
        return res.redirect(`${req.baseUrl}${nextNav}`); 
      }
    }catch (exception) {
      console.error('exception error -->', exception);      
      
    }
  }

};