const Controller = require('hof-govfrontend-v3').controller;
module.exports = class DependantFileDetailsController extends Controller {

  getValues(req, res, callback) {
    const field = this.options.locals.field;
    super.getValues(req, res, callback);
  }

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    locals.ehicUploadFile = req.sessionModel.get('dependant-ehic');
    locals.ehicFileUrl = 'dependant-ehic';
    const depEhicFile = req.sessionModel.get('dependant-ehic');
    req.sessionModel.set('depEhicFile', [{ srNo: 1, fileName: depEhicFile['fileName'], fileId: depEhicFile['fileId'] }]);
    const allDependants = req.sessionModel.get('all-dependants');
    const dependantId = req.sessionModel.get('dependant-id');

    if (dependantId && allDependants && allDependants.length != 0) {
      const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === dependantId);
      if (dependantIndex != -1) {
        allDependants[dependantIndex]['dependant-ehic'] = req.sessionModel.get('dependant-ehic');
        allDependants[dependantIndex]['upload-dependant-european-health-insurance-card-ehic'] = req.sessionModel.get('dependant-ehic');
        req.sessionModel.set('all-dependants', allDependants);
      }
    }
    locals.maxFileUpload = req.sessionModel.get('maxFileUpload');
    locals.ehicDeleteErrorMsg = req.sessionModel.get('ehicDeleteErrorMsg') ? req.sessionModel.get('ehicDeleteErrorMsg') : false;
    locals.deleteErrorFileType = req.sessionModel.get('error-file-type') ? req.sessionModel.get('error-file-type')  : 'delete-ehic-file';
    req.sessionModel.unset('maxFileUpload');
    req.sessionModel.unset('ehicDeleteErrorMsg');
    return locals;
  }

  process(req, res, next) {  
    const dependantId = req.sessionModel.get('edit-dependantId');
    if(dependantId && req.sessionModel.get('check-your-answers')){
        return res.redirect(`${req.baseUrl}/check-your-answers`);
    }else{
      next();
    }
  }

};