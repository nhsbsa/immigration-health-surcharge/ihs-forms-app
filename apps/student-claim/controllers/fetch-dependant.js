const Controller = require('hof-govfrontend-v3').controller;
const PersonalDetailsModel = require('../models/personal-details');
const DependantModel = require('./../models/dependant');

module.exports = class DependantAddController extends Controller {

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    const dependantId = req.sessionModel.get('edit-dependantId');
    if(dependantId){
      const allDependants = req.sessionModel.get('all-dependants');  
      const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === dependantId);   
       locals.fields.forEach((formfield, index) => {
        if(req.form['errors'][formfield.key]){
          req.form.values[formfield.key] = req.form.values[formfield.key];  
        }else if(req.form.values[formfield.key]){
          req.form.values[formfield.key] = req.form.values[formfield.key];  
        }else{          
          req.form.values[formfield.key] = allDependants[dependantIndex][formfield.key];  
        } 
      });
    } 
    return locals;
  }

  copyDepedantAddress(req,dependantIndex,allDependants){
    const personalDetails = new PersonalDetailsModel(req.sessionModel.toJSON());
    allDependants[dependantIndex]['dependant-enter-address'] = personalDetails['enter-address'];
    allDependants[dependantIndex]['dependant-address-line-1'] = personalDetails['address-line-1'];
    allDependants[dependantIndex]['dependant-address-line-2'] = personalDetails['address-line-2'];
    allDependants[dependantIndex]['dependant-address-town'] = personalDetails['address-town'];
    allDependants[dependantIndex]['dependant-address-county'] = personalDetails['address-county'];
    allDependants[dependantIndex]['dependant-address-postcode'] = personalDetails['address-postcode'];
    return allDependants;
  }

  process(req, res, next) {  
    next();
  }

  successHandler(req, res, next) {
    const dependantId = req.sessionModel.get('edit-dependantId');
    let  takeApplicantAddress = false;
    if(dependantId){
      let allDependants = req.sessionModel.get('all-dependants');
      const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === dependantId);  
      const locals = super.locals(req, res,next);
      locals.fields.forEach((formfield, index) => {  
        allDependants[dependantIndex][formfield.key] =req.form.values[formfield.key] ;  
         if(formfield.key == 'dependant-same-address-as-applicant' && req.form.values['dependant-same-address-as-applicant'] === 'yes' ){    
          allDependants = this.copyDepedantAddress(req,dependantIndex,allDependants); 
          takeApplicantAddress = true;
        }
      });   
      if(req.form.options.route == '/dependant-enter-address'){
        allDependants[dependantIndex]['dependant-same-address-as-applicant'] = 'no';
      }
      allDependants[dependantIndex]['dependant-ehic'] =allDependants[dependantIndex]['upload-dependant-european-health-insurance-card-ehic'];
      const depmodel =  new DependantModel(allDependants[dependantIndex],takeApplicantAddress);
      takeApplicantAddress = false;
      allDependants[dependantIndex] = depmodel.editDependant(depmodel,takeApplicantAddress);
      req.sessionModel.set('all-dependants', allDependants);
     
      if(req.sessionModel.get('depFileCount-'+dependantId) <= 0){        
        req.sessionModel.set('no-dep-file', true);
      }
    
      if(allDependants[dependantIndex]['dependant-ehic']){
        req.sessionModel.unset('dependant-id');
      }      
    } 
    
    if(dependantId){
      if((req.sessionModel.get('check-dependant-answers') || req.sessionModel.get('check-your-answers') ) 
          && req.sessionModel.get('no-dep-file')){
            req.sessionModel.unset('no-dep-file');
            return res.redirect(`${req.baseUrl}/upload-dependant-european-health-insurance-card-ehic`);
      }
      req.sessionModel.unset('edit-dependantId');
      if(req.sessionModel.get('check-dependant-answers')){
        req.sessionModel.unset('check-dependant-answers');
        const nextNav = '/check-dependant-answers';
        req.form.options['confirmStep'] = nextNav;
        return res.redirect(`${req.baseUrl}${nextNav}`);
      }else{
        if(req.sessionModel.get('check-your-answers') &&  req.form.values['dependant-same-address-as-applicant'] === 'no'){
          const nextNav = '/check-your-answers';
          req.sessionModel.unset('check-dependant-answers');
          req.sessionModel.unset('check-your-answers');      
          return res.redirect(`${req.baseUrl}${nextNav}`); 
        }           
      }
    }
    return super.successHandler(req, res, next);    
  }  
};