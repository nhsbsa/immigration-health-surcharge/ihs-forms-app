const Controller = require('hof-govfrontend-v3').controller;
const { notEligibleClearSession } = require('./../constants/dependant');
module.exports = class DependantNotEligibleController extends Controller {

  process(req, res, next) {
    req.sessionModel.unset(notEligibleClearSession);
    next();
  }

};