const Controller = require('hof-govfrontend-v3').controller;
module.exports = class FileDetailsController extends Controller {

  getValues(req, res, callback) {
    const field = this.options.locals.field;
    super.getValues(req, res, callback);
  }

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    locals.ehicUploadFile = req.sessionModel.get('ehicFile') ? req.sessionModel.get('ehicFile') : false;
    locals.ehicFileUrl = 'ehicFile';
    locals.studyLetterUploadFile = req.sessionModel.get('studyLetter');
    locals.ehicDeleteErrorMsg = req.sessionModel.get('ehicDeleteErrorMsg') ? req.sessionModel.get('ehicDeleteErrorMsg') : false;
    locals.deleteErrorFileType = req.sessionModel.get('error-file-type') ? req.sessionModel.get('error-file-type')  : 'delete-ehic-file';
    locals.maxFileUpload = req.sessionModel.get('maxFileUpload');
    req.sessionModel.unset('maxFileUpload');
    req.sessionModel.unset('ehicDeleteErrorMsg');
    return locals;
  }
  
  process(req, res, next) {     
   if(req.sessionModel.get('check-your-answers') && req.sessionModel.get('studyLetterCount') > 0){
    return res.redirect(`${req.baseUrl}/check-your-answers`);
   }else{
    next();
   }   
  }
};