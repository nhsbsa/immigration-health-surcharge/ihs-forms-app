const Controller = require('hof-govfrontend-v3').controller;
const DependantModel = require('./../models/dependant');
const dependantSections = require('./../sections/dependant')
const moment = require('moment');
const { moreDependantClearSession, maxDependantCount } = require('./../constants/dependant');

const radioinputs = ['add-dependants-with-european-health-insurance-card-ehic',
  'dependant-paid-work-uk',
  'dependant-living-in-uk',
  'dependant-date-of-birth',
  'upload-dependant-european-health-insurance-card-ehic'
];
const inputques = [
  'immigration-health-surcharge-number',
  'applicant-share-code',
  'dependant-immigration-health-surcharge-number',
  'dependant-share-code'
];
module.exports = class DependantDetailsController extends Controller {

  getValues(req, res, callback) {
    req.sessionModel.set('check-dependant-answers', true);
    req.sessionModel.unset('check-your-answers');
    req.sessionModel.unset('edit-dependantId');
    const field = this.options.locals.field;
    super.getValues(req, res, callback);
    const sessionModel = req.sessionModel.toJSON();
    this.saveDependant(req, sessionModel);
  }

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    const allDependants = req.sessionModel.get('all-dependants');
    locals.dependantsDataList = this.genrateDependantDataList(req, allDependants)
    const deletedDependant = req.sessionModel.get('deletedDependant');
    locals.deletedDependant = deletedDependant ? deletedDependant : false;
    req.sessionModel.unset('deletedDependant');
    return locals;
  }

  saveDependant(req, sessionModel) {
    const currentDependantId = sessionModel['dependant-id'];
   
    if(!currentDependantId) return;
    let allDependants = sessionModel['all-dependants'] ? sessionModel['all-dependants'] : [];
    const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === currentDependantId)
    if(dependantIndex === -1) {
      allDependants.push(new DependantModel(sessionModel))
    }
    req.sessionModel.set('all-dependants', allDependants);
  }

  genrateDependantDataList(req, allDependants) {
    const dependantDataList = [];
    allDependants.forEach((dependant, index) => {
      const dependantData = { dependantIndex: index + 1, dependantId: dependant['dependant-id']};
      const notDisplayChangeLinks = ["applicant-file-upload",  "dependant-paid-work-uk", "dependant-living-in-uk","add-dependants-with-european-health-insurance-card-ehic"];
      dependantData.dependantFields = dependantSections.map(key => {
        const value = dependant[key];       
        const url = (notDisplayChangeLinks.includes(key))? false: (key == 'upload-dependant-european-health-insurance-card-ehic')?'uploaded-dependant-files-european-health-insurance-card-ehic':key ;
        return {
          label: inputques.includes(key) ? req.translate(`fields.${key}.last-label`): req.translate(`fields.${key}.label`),
          value: radioinputs.includes(key) ? this.getDisplayValue(req, key, value): value,
          url: url,
          key:key

        }
      })
      dependantDataList.push(dependantData)
    });
    return dependantDataList;
  }

  getDisplayValue(req, key, value) { 
    if(key === 'dependant-date-of-birth'){
      return moment(value, 'YYYY-MM-DD').format('DD MMMM YYYY');
    }
    if(key == 'upload-dependant-european-health-insurance-card-ehic'){        
      return value[0].fileName;
    }
    return req.translate(`fields.${key}.options.${value}.label`);
  }

  process(req, res, next) {
    const allDependants = req.sessionModel.get('all-dependants');
    const currentStep = 'add-more-dependants';
    const dependantLength = allDependants ? allDependants.length : 0;
    const moreDependants = req.form.values[currentStep];
    const uniqueId = moreDependants === 'yes' ? ('id' + (new Date()).getTime()) : void 0;
    if (dependantLength >= maxDependantCount && moreDependants === 'yes') {
      const err = new this.ValidationError(currentStep, {
        type: 'maxDependants'
      }, req, res);
      const errorObject = {};
      errorObject[currentStep] = err;
      return next(errorObject);
    }
    if(moreDependants) {
      req.sessionModel.unset(moreDependantClearSession);
      if(moreDependants === 'yes') {
        req.sessionModel.set('add-dependants-with-european-health-insurance-card-ehic', moreDependants);
        req.sessionModel.set('dependant-id', uniqueId);
        const nextNav = '/dependant-paid-work-uk';
        return res.redirect(`${req.baseUrl}${nextNav}`);
      }      
    }    
    next();
  }
  
};