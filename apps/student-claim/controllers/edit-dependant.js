const Controller = require('hof-govfrontend-v3').controller;
const { ValidationError } = require('hof-govfrontend-v3/controller');
module.exports = class DependantAddController extends Controller {

  get(req, res, next) {
    const dependantId = req.query.dependantId;
    const url = req.query.pagename;
    if (dependantId) {
      this.editDependant(req, res, dependantId,url);
    }else{
      next(new ValidationError('Invalid dependantId.'));
    }
  }

  editDependant(req, res, dependantId,url) {
    const allDependants = req.sessionModel.get('all-dependants');
    req.sessionModel.set('edit-dependantId',dependantId);
    const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === dependantId); 
    if (dependantIndex !== -1) {
      for (let key in allDependants[dependantIndex]){
        allDependants[dependantIndex].hasOwnProperty(key) ?  req.sessionModel.set(key,allDependants[dependantIndex][key]) : false;
      }    
    }
    const nextNav = '/'+url+'?dependantId='+dependantId;   
    return res.redirect(`${req.baseUrl}${nextNav}`); 
  }  
};