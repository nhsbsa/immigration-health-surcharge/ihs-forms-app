const Controller = require('hof-govfrontend-v3').controller;
module.exports = class DependantAddController extends Controller {

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    const deletedDependant = req.sessionModel.get('deletedDependant');
    locals.deletedDependant = deletedDependant ? deletedDependant : false;
    req.sessionModel.unset('deletedDependant');
    return locals;
  }

  process(req, res, next) {
    const moreDependants = req.form.values['add-dependants-with-european-health-insurance-card-ehic'];
    const uniqueId = moreDependants === 'yes' ? ('id' + (new Date()).getTime()) : void 0;
    req.sessionModel.set('dependant-id', uniqueId);
    next();
  }
  
};