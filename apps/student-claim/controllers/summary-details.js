const Controller = require('hof-govfrontend-v3').controller;
const {dependantSections, personalDetailsSection} = require('./../sections/personalDetails');
const PersonalDetailsModel = require('../models/personal-details');
const moment = require('moment');

const radioinputs = [
  'add-dependants-with-european-health-insurance-card-ehic',
  'dependant-paid-work-uk',
  'dependant-living-in-uk',
  'dependant-date-of-birth',
  'date-of-birth',
  'applicant-file-upload',
  'upload-dependant-european-health-insurance-card-ehic'
];
const inputques = [
  'immigration-health-surcharge-number',
  'applicant-share-code',
  'dependant-immigration-health-surcharge-number',
  'dependant-share-code'
];

module.exports = class SummaryDetailsController extends Controller {

  getValues(req, res, callback) {
    req.sessionModel.set('check-your-answers', true);
    req.sessionModel.unset('check-dependant-answers');
    req.sessionModel.unset('edit-dependantId');
    const field = this.options.locals.field;
    super.getValues(req, res, callback);
    const sessionModel = req.sessionModel.toJSON();
    this.savePersonalDetails(req, sessionModel);
  }

  locals(req, res, callback) {
    const locals = super.locals(req, res, callback);
    const allDependants = req.sessionModel.get('all-dependants');
    const personalDetails = req.sessionModel.get('personalDetails');
    locals.dependantsDataList = allDependants ? this.genrateDependantDataList(req, allDependants) : [];
    locals.personalDetailsDataList = this.generatePersonalDataList(req, personalDetails);
    return locals;
  }

  savePersonalDetails(req, sessionModel) {
    const personalDetails = new PersonalDetailsModel(sessionModel);
    req.sessionModel.set('personalDetails', personalDetails);
  }

  generatePersonalDataList(req, personalDetails) {   
    return personalDetailsSection.map(key => {
      const value = personalDetails[key];
      const url = this.getUrlLink(key);
      return value ? {
          label: inputques.includes(key) ? req.translate(`fields.${key}.last-label`): req.translate(`fields.${key}.label`),
          value: radioinputs.includes(key) ? this.getDisplayValue(req, key, value): value,
          url: url,
          key:key
        } : void 0;      
    }).filter(item => item);
  }

  genrateDependantDataList(req, allDependants) {  
    const dependantDataList = [];
    allDependants.forEach((dependant, index) => {
      const dependantData = { dependantIndex: index + 1, dependantId: dependant['dependant-id']};
      dependantData.dependantFields = dependantSections.map(key => {
        const value = dependant[key];
        const url = this.getUrlLink(key);
        return {
          label: inputques.includes(key) ? req.translate(`fields.${key}.last-label`): req.translate(`fields.${key}.label`),
          value: radioinputs.includes(key) ? this.getDisplayValue(req, key, value): value,      
          url: url,
          key:key
        }
      })
      dependantDataList.push(dependantData);
    }); 
    return dependantDataList;
  }
  getUrlLink(key){
    if(key ==  'upload-dependant-european-health-insurance-card-ehic'){
      return 'uploaded-dependant-files-european-health-insurance-card-ehic';
    }else if(key == 'applicant-file-upload' ){
      return 'uploaded-files-evidence';
    }else{
      return key;
    }
  }
  getDisplayValue(req, key, value) {
    switch (key) {
      case 'dependant-date-of-birth':
      case 'date-of-birth':
        return moment(value, 'YYYY-MM-DD').format('DD MMMM YYYY');
      case 'applicant-file-upload':        
        return value[0][0]['fileName']+"<br>"+value[1][0]['fileName'];
      case 'upload-dependant-european-health-insurance-card-ehic':
        return value[0].fileName;
      default:
        return req.translate(`fields.${key}.options.${value}.label`);
    }    
  }

  getFieldName(req, key, value) {
    switch (key) {
      case 'dependant-date-of-birth':
      case 'date-of-birth':
        return moment(value, 'YYYY-MM-DD').format('DD MMMM YYYY');
      case 'applicant-file-upload':
        return value.map(fileName => `${fileName}`).join('<br>');
      case 'upload-dependant-european-health-insurance-card-ehic':
        return `${value}`;
      default:
        return req.translate(`fields.${key}.options.${value}.label`);
    }    
  }
  
};