const Controller = require('hof-govfrontend-v3').controller;
const { ValidationError } = require('hof-govfrontend-v3/controller');
const FileUploadModel = require('../models/file-upload');
const fileUploadModel = new FileUploadModel();
module.exports = class EhicFileDeleteController extends Controller {

    async get(req, res, next) {
        try {
            const fileId = req.query.fileId;
            const fileType = req.query.fileType;
            const navigate = req.query.navigate;

            if (req.sessionModel.get('student-claim-submit-flag'))
                throw new ValidationError('Invalid request.');


            if (!fileId)
                throw new ValidationError('Invalid file id.');

            let fileUploadRequestObj = await fileUploadModel.fileDeleteApi(req.sessionModel.get('claim'), fileId);
            let redirectUrl = '';
            let UploadedFiles = req.sessionModel.get(fileType);
            if (fileType === 'ehicFile') {
                redirectUrl = `${req.baseUrl}/upload-european-health-insurance-card-ehic`;
            } else if (fileType === 'studyLetter') {
                redirectUrl = `${req.baseUrl}/upload-evidence`;
            } else if (fileType === 'dependant-ehic') {
                redirectUrl = `${req.baseUrl}/upload-dependant-european-health-insurance-card-ehic`;
            }
            if (typeof fileUploadRequestObj.data == undefined || fileUploadRequestObj.data.status != 200) {
                req.sessionModel.set('ehicDeleteErrorMsg', `${UploadedFiles[0]['fileName']} was not deleted. Try again`);
                let errorFileType;                
                if (fileType === 'ehicFile') {
                    errorFileType = 'delete-ehic-file';
                    redirectUrl = `${req.baseUrl}/${navigate}`;
                } else if (fileType === 'studyLetter') {
                    errorFileType = 'delete-cas-file';
                    redirectUrl = `${req.baseUrl}/${navigate}`;
                } else if (fileType === 'dependant-ehic') {
                    errorFileType = 'delete-dependant-ehic';
                    redirectUrl = `${req.baseUrl}/uploaded-dependant-files-european-health-insurance-card-ehic`;
                }
                req.sessionModel.set('error-file-type',errorFileType);
            } else {
                if (typeof (UploadedFiles) != 'undefined') {
                    req.sessionModel.set('ehicDeleteInfo', UploadedFiles[0]['fileName']);
                }
                req.sessionModel.unset(fileType);
                this.decrementEvidenceCount(fileType, req);
            }
            return res.redirect(redirectUrl);

        } catch (exception) {
            console.error('exception error -->', exception);
            if (exception.customeException) {
                //TODO: NEED TO WORK ON THE CUSTOME EXCEPTION
                next();
            } else {
                next(exception);
            }
        }
    }

    decrementEvidenceCount(fileNameKey, req) {
        if (fileNameKey === 'ehicFile') {
            //Decrease main file count
            const ehicFileCount = req.sessionModel.get('ehicFileCount');
            req.sessionModel.set('ehicFileCount', ehicFileCount ? ehicFileCount - 1 : 0);
        } else if (fileNameKey === 'studyLetter') {
            //Decrease main file count
            const studyLetterCount = req.sessionModel.get('studyLetterCount');
            req.sessionModel.set('studyLetterCount', studyLetterCount ? studyLetterCount - 1 : 0);
        } else {
            //Decrease depedent file count         
            let depFilecount = "depFileCount-" + req.sessionModel.get('dependant-id');
            const depFileCountFromSession = req.sessionModel.get(depFilecount);
            req.sessionModel.set(depFilecount, depFileCountFromSession ? depFileCountFromSession - 1 : 0);
        }
    }
}