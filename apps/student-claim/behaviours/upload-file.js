'use strict';
const path = require('path');
const ClaimUtility = require('../utility/claim-utility');
const FileUploadModel = require('../models/file-upload');
const fileUploadModel = new FileUploadModel();

module.exports = superclass => class UploadFile extends superclass {
  async process(req, res, next) {
    try {
      const locals = super.locals(req, res, next);
      const currentStep = locals.step;
      if (req.sessionModel.get('student-claim-submit-flag'))
        throw { status: false, validationType: '400', currentStep: currentStep };

      const validationObj = this.checkValidation(req, res, next, currentStep);
      if (!validationObj.status)
        throw validationObj;
      req.files[currentStep].name = req.files[currentStep].name.replace(/ /g, "_");
      let fileUploadObj = await this.fileupload(req, currentStep, next);
      if ((!fileUploadObj) || (!fileUploadObj.data.status) || fileUploadObj.data.status != '200') {
        console.error('Error occured while uploading File, Please try again later! ' );
        return this.flashError(req, res, next, currentStep, "error");
      } else {
        this.incrementEvidenceCount(req, locals, currentStep, fileUploadObj['data']);
        if (req.sessionModel.get('studyLetter') && currentStep == 'upload-european-health-insurance-card-ehic') {
          return res.redirect(`${req.baseUrl}/uploaded-files-evidence`);
        } else {
          next();
        }
      }
    } catch (error) {
      if (error.validationType == 'required') {
        return next({
          [error.currentStep]: new this.ValidationError(error.currentStep, {
            type: (error.validationType == 'error' || typeof error.validationType === 'undefined') ? 'error' : error.validationType
          }, req, res)
        });
      } else if (error.validationType == '400') {
        return next(new this.ValidationError('Invalid request.'));
      } else {
        req.sessionModel.set('evidenceUploadErrorMsg', error.message);
        return res.redirect(`${req.baseUrl}/${error.currentStep}`);
      }
    }
  }

  incrementEvidenceCount(req, locals, currentStep, fileData) {
    if (locals.fileNameKey === 'ehicFile') {
      //Increase main file count
      const ehicFileCount = req.sessionModel.get('ehicFileCount');
      req.sessionModel.set('ehicFileCount', ehicFileCount ? ehicFileCount + 1 : 1);
    } else if (locals.fileNameKey === 'studyLetter') {
      //Increase main file count
      const studyLetterCount = req.sessionModel.get('studyLetterCount');
      req.sessionModel.set('studyLetterCount', studyLetterCount ? studyLetterCount + 1 : 1);
    } else {
      //increase depedent file count 
      let depFilecount = "depFileCount-" + req.sessionModel.get('dependant-id');
      const depFileCountFromSession = req.sessionModel.get(depFilecount);
      req.sessionModel.set(depFilecount, depFileCountFromSession ? depFileCountFromSession + 1 : 1);

      const allDependants = req.sessionModel.get('all-dependants');
      const dependantId = req.sessionModel.get('dependant-id');

      if (dependantId && allDependants && allDependants.length != 0) {
        const dependantIndex = allDependants.findIndex(dependant => dependant['dependant-id'] === dependantId);
        if (dependantIndex != -1) {
          allDependants[dependantIndex]['dependant-ehic'] = req.sessionModel.get('dependant-ehic');
          allDependants[dependantIndex]['upload-dependant-european-health-insurance-card-ehic'] = req.sessionModel.get('dependant-ehic');
          req.sessionModel.set('all-dependants', allDependants);
        }
      }
    }
    req.sessionModel.set(currentStep, req.files[currentStep].name);
    req.sessionModel.set(locals.fileNameKey, [{ srNo: 1, fileName: req.files[currentStep].name, fileId: fileData['fileid'] }]);
  }

  flashError(req, res, next, currentStep, validationType) {
    const err = new this.ValidationError(currentStep, {
      type: validationType
    }, req, res);
    const errorObject = {};
    errorObject[currentStep] = err;
    return next(errorObject);
  }

  checkFileCount(req, res, next, currentStep) {
    if (((req.url === '/upload-dependant-european-health-insurance-card-ehic')
      && (req.sessionModel.get("depFileCount-" + req.sessionModel.get('dependant-id')) >= process.env.MAX_FILE_COUNT))
      || (((req.url === '/upload-evidence')
        && req.sessionModel.get('studyLetterCount') >= process.env.MAX_FILE_COUNT))
      || (((req.url === '/upload-european-health-insurance-card-ehic')
        && req.sessionModel.get('ehicFileCount') >= process.env.MAX_FILE_COUNT)
      )
    ) {
      req.sessionModel.set('maxFileUpload', true);
      return res.redirect(`${req.baseUrl}${req.form.options.next}`);
    }
    return true;
  }

  checkValidation(req, res, next, currentStep) {
    const file = req.files[currentStep];

    if (file == null) {
      return { status: false, validationType: 'error', currentStep: currentStep, message: `Select a file to upload as evidence` };
    }

    const fileCountFlag = this.checkFileCount(req, res, next, currentStep);
    if (!fileCountFlag) {
      return false;
    }
    const pattern = /^[a-z0-9\-_\s]+$/i;
    const extension = path.extname(file.name);
    var filename = path.basename(file.name, extension);

    if (!fileCountFlag) {
      return { status: false, validationType: 'error', currentStep: currentStep, message: `${filename}${extension} was not uploaded. Try again` };
    }

    if (!pattern.test(filename)) {
      return { status: false, validationType: 'regex', currentStep: currentStep, message: `${filename}${extension} must use only letters, numbers, spaces, hyphens and underscore` };
    }

    if (!this.fetchExt(file.name)) {
      return { status: false, validationType: 'extname', currentStep: currentStep, message: `${filename}${extension} must be a jpg, bmp, png or pdf` };
    }

    if (file && file.truncated) {
      return { status: false, validationType: 'filesize', currentStep: currentStep, message: `${filename}${extension} must be less than 2MB in size` };
    }

    if (file.size > process.env.FILE_SIZE_LIMIT) {
      return { status: false, validationType: 'filesize', currentStep: currentStep, message: `${filename}${extension} must be less than 2MB in size` };
    }
    
    if (filename.length > Number(process.env.MAX_FILENAME_LENGTH )) {
      return { status: false, validationType: 'filelength', currentStep: currentStep, message: `${filename}${extension} must be 130 characters or less` };
    }

    return { status: true, validationType: 'filesize', currentStep: currentStep };
  }

  locals(req, res, next) {
    const locals = super.locals(req, res, next);
    const currentStep = locals.step;
    if (!Object.keys(req.form.errors).length) {
      req.form.values[currentStep] = null;
    }
    locals.ehicDeleteInfo = req.sessionModel.get('ehicDeleteInfo') ? req.sessionModel.get('ehicDeleteInfo') : false;
    res.locals.errorLogMsg = req.sessionModel.get('evidenceUploadErrorMsg') ? req.sessionModel.get('evidenceUploadErrorMsg') : false;
    res.locals.pageName = req.url.slice(1);
    req.sessionModel.unset('evidenceUploadErrorMsg');
    req.sessionModel.unset('ehicDeleteInfo');
    return locals;
  }

  async fileupload(req, currentStep, next) {
    const claim = req.sessionModel.get('claim');
    let file = ClaimUtility.fileData(req.files[currentStep]);
    return fileUploadModel.fileuploadAPI(claim, file, next);
  }

  fetchExt(value) {
    return value && [
      '.jpeg',
      '.jpg',
      '.bmp',
      '.png',
      '.pdf'
    ].includes(path.extname(value).toLowerCase());
  }

};
