'use strict';

const personalDetailsSection = [
  'name',
  'date-of-birth',
  'immigration-health-surcharge-number',
  'applicant-share-code',
  'enter-address',
  'email-address',
  'telephone-number',
  'applicant-file-upload'
];

const dependantSections = [
  'dependant-name',
  'dependant-date-of-birth',
  'dependant-immigration-health-surcharge-number',
  'dependant-share-code',
  'dependant-enter-address',
  'upload-dependant-european-health-insurance-card-ehic'
];

module.exports = {
  dependantSections,
  personalDetailsSection
};
