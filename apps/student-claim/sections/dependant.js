'use strict';

module.exports = [
  'add-dependants-with-european-health-insurance-card-ehic',
  'dependant-paid-work-uk',
  'dependant-living-in-uk',
  'dependant-name',
  'dependant-date-of-birth',
  'dependant-immigration-health-surcharge-number',
  'dependant-share-code',
  'dependant-enter-address',
  'upload-dependant-european-health-insurance-card-ehic'
];
