const Controller = require('hof-govfrontend-v3').controller;


module.exports = class FooterController extends Controller {

  getValues(req, res, callback) {
    super.getValues(req, res, callback);   
  }

  locals(req, res, callback) {
    
    let pageName = req.form.options.template;
    const locals = super.locals(req, res, callback);   
    const footerJson =  require('./../translations/src/en/'+pageName+'.json');
    const jsonData = footerJson;
    locals.jsonData = jsonData;
    return locals;
  }
}