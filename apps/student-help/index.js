'use strict';
const path = require('path');

module.exports = {
  name: 'student-help',
  baseUrl: '/student-help',
  views: path.resolve(__dirname, './views'),
 
  steps: {
    '/privacy-notice': {
      controller: require('./controllers/footer-page')
    },
    '/accessibility-statement': {
      controller: require('./controllers/footer-page')
    },
    '/terms-conditions':{
      controller: require('./controllers/footer-page')
    },
    '/contact': {
      controller: require('./controllers/footer-page')
    },
    '/cookies':{
      controller: require('./controllers/footer-page')
    },


  }
};
