FROM public.ecr.aws/docker/library/node:16-alpine

ARG NODE_ENV=development

USER root

RUN ln -fs /usr/share/zoneinfo/Europe/London /etc/localtime

# Remove SSHD
RUN rm -rf /etc/service/sshd /etc/my_init.d/00_regen_ssh_host_keys.sh
	
# Setup nodejs group & nodejs user
RUN addgroup --system nodejs --gid 998 && \
    adduser --system nodejs --uid 999 --home /app/ && \
    chown -R 999:998 /app/

ENV NODE_ENV=${NODE_ENV}

USER nodejs

WORKDIR /usr/src/app
COPY ["package.json", "package-lock.json*", "./"]
COPY --chown=nodejs:nodejs . /usr/src/app

RUN npm install
	
HEALTHCHECK --interval=5m --timeout=3s \
    CMD curl --fail http://localhost:8080 || exit 1

CMD [ "node", "server.js" ]

ENV BSA_URL=bsa-url
ENV IHS_API_KEY=ihs-api-key
ENV GA_TAG=bsa-url
ENV SESSION_TIMEOUT=300
ENV SESSION_TIMEOUT_WARNING=180
ENV FINAL_SESSION_TIMEOUT_WARNING=60

EXPOSE 8080
